-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2014 at 08:38 PM
-- Server version: 5.5.36-MariaDB-log
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trip`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `commentid` varchar(200) NOT NULL,
  `tripid` varchar(200) NOT NULL,
  `userid` varchar(20) NOT NULL,
  `comment` text NOT NULL,
  `commenteddatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `locationid` varchar(40) DEFAULT NULL,
  `photoid` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`commentid`),
  KEY `fk_trip_has_user1_user2_idx` (`userid`),
  KEY `fk_trip_has_user1_trip2_idx` (`tripid`),
  KEY `fk_comments_location1_idx` (`locationid`),
  KEY `fk_comments_photo1_idx` (`photoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE IF NOT EXISTS `event_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '		',
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `userid` varchar(20) NOT NULL,
  `tripid` varchar(200) NOT NULL,
  PRIMARY KEY (`userid`,`tripid`),
  KEY `fk_user_has_trip_trip2_idx` (`tripid`),
  KEY `fk_user_has_trip_user2_idx` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `locationid` varchar(40) NOT NULL,
  `name` text,
  `lon` double(20,18) NOT NULL,
  `lat` double(20,18) NOT NULL,
  `description` text,
  `address` text NOT NULL,
  `visitdate` datetime DEFAULT NULL,
  `Trip` varchar(200) NOT NULL,
  PRIMARY KEY (`locationid`),
  KEY `fk_location_Trip1_idx` (`Trip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `location_has_tag`
--

CREATE TABLE IF NOT EXISTS `location_has_tag` (
  `location_locationid` varchar(40) NOT NULL,
  `tag_tagid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`location_locationid`,`tag_tagid`),
  KEY `fk_location_has_tag_tag1_idx` (`tag_tagid`),
  KEY `fk_location_has_tag_location1_idx` (`location_locationid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `newuser`
--

CREATE TABLE IF NOT EXISTS `newuser` (
  `userid` varchar(20) NOT NULL,
  `homepagevisit` tinyint(1) DEFAULT '0',
  `addeventvisit` tinyint(1) DEFAULT '0',
  `eventshowvisit` tinyint(1) DEFAULT '0',
  `profilevisit` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`userid`),
  KEY `fk_newuser_user1_idx` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `photoid` varchar(40) NOT NULL,
  `url` text NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` text,
  `iscoverphoto` tinyint(1) NOT NULL DEFAULT '0',
  `location` varchar(40) DEFAULT NULL,
  `Trip` varchar(200) NOT NULL,
  `user` varchar(20) NOT NULL,
  PRIMARY KEY (`photoid`),
  KEY `fk_photo_location1_idx` (`location`),
  KEY `fk_photo_Trip1_idx` (`Trip`),
  KEY `fk_photo_user1_idx` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `tagid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`tagid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trip`
--

CREATE TABLE IF NOT EXISTS `trip` (
  `tripid` varchar(200) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` longtext NOT NULL,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `coverphoto` text,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `publish_type` enum('PUBLIC','FRIEND','MEMBER','CUSTOME','ONLYME') NOT NULL DEFAULT 'PUBLIC',
  `updatedatetime` datetime DEFAULT NULL,
  `coverphoto_top` int(11) NOT NULL DEFAULT '0',
  `event_type` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tripid`),
  KEY `fk_trip_event_type1_idx` (`event_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trip_has_tag`
--

CREATE TABLE IF NOT EXISTS `trip_has_tag` (
  `Trip_tripid` varchar(200) NOT NULL,
  `tag_tagid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Trip_tripid`,`tag_tagid`),
  KEY `fk_Trip_has_tag_tag1_idx` (`tag_tagid`),
  KEY `fk_Trip_has_tag_Trip1_idx` (`Trip_tripid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trip_has_user`
--

CREATE TABLE IF NOT EXISTS `trip_has_user` (
  `Trip` varchar(200) NOT NULL,
  `User` varchar(20) NOT NULL,
  `position` enum('MEMBER','VISITOR','ORGANIZER','PUBLISHER','BLOCK','OWNER') NOT NULL,
  PRIMARY KEY (`Trip`,`User`),
  KEY `fk_Trip_has_User_User1_idx` (`User`),
  KEY `fk_Trip_has_User_Trip_idx` (`Trip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `updatehistory`
--

CREATE TABLE IF NOT EXISTS `updatehistory` (
  `userid` varchar(20) NOT NULL,
  `tripid` varchar(200) NOT NULL,
  `updatedatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(45) NOT NULL DEFAULT 'Update',
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userid`,`tripid`,`updatedatetime`),
  KEY `fk_user_has_trip_trip1_idx` (`tripid`),
  KEY `fk_user_has_trip_user1_idx` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userid` varchar(20) NOT NULL,
  `username` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `confirm` varchar(255) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `fbid` varchar(255) DEFAULT NULL,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `propic` text,
  `gender` enum('male','female') NOT NULL DEFAULT 'male',
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `watchlater`
--

CREATE TABLE IF NOT EXISTS `watchlater` (
  `tripid` varchar(200) NOT NULL,
  `userid` varchar(20) NOT NULL,
  `addeddatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tripid`,`userid`),
  KEY `fk_trip_has_user1_user1_idx` (`userid`),
  KEY `fk_trip_has_user1_trip1_idx` (`tripid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fk_comments_location1` FOREIGN KEY (`locationid`) REFERENCES `location` (`locationid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_comments_photo1` FOREIGN KEY (`photoid`) REFERENCES `photo` (`photoid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_trip_has_user1_trip2` FOREIGN KEY (`tripid`) REFERENCES `trip` (`tripid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_trip_has_user1_user2` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `fk_user_has_trip_trip2` FOREIGN KEY (`tripid`) REFERENCES `trip` (`tripid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_has_trip_user2` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `location`
--
ALTER TABLE `location`
  ADD CONSTRAINT `fk_location_Trip1` FOREIGN KEY (`Trip`) REFERENCES `trip` (`tripid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `location_has_tag`
--
ALTER TABLE `location_has_tag`
  ADD CONSTRAINT `fk_location_has_tag_location1` FOREIGN KEY (`location_locationid`) REFERENCES `location` (`locationid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_location_has_tag_tag1` FOREIGN KEY (`tag_tagid`) REFERENCES `tag` (`tagid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `newuser`
--
ALTER TABLE `newuser`
  ADD CONSTRAINT `fk_newuser_user1` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trip`
--
ALTER TABLE `trip`
  ADD CONSTRAINT `fk_trip_event_type1` FOREIGN KEY (`event_type`) REFERENCES `event_type` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `trip_has_user`
--
ALTER TABLE `trip_has_user`
  ADD CONSTRAINT `fk_Trip_has_User_User1` FOREIGN KEY (`User`) REFERENCES `user` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Trip_has_User_Trip` FOREIGN KEY (`Trip`) REFERENCES `trip` (`tripid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;