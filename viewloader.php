<?php
include_once 'index.php';
/**
 * Created by PhpStorm.
 * User: dewmal
 * Date: 3/14/14
 * Time: 6:54 AM
 */
$app->get('/', function () use ($app) {
    $phtoCtrl=new \App\PhotCtrl();
    $randomPhotos=$phtoCtrl->getRandom(10);
    $app->render('index.php',array("randomPhotos"=>$randomPhotos));
});

$app->get('/fbauth/', function () use ($app) {

    $app->redirect("/userauth");
});

$app->get('/:pageid', function ($pageid) use ($app) {


    if ($pageid == 'userauth') {

        //echo "Working viewloader";
        //print_r($_SESSION);
        //echo("<p></p>" . array_key_exists("userid", $_SESSION));

        if (!array_key_exists("user", $_SESSION) ||!is_array($_SESSION['user'])) {
            $redirectPage = "/";
            $invalid = false;
            if (array_key_exists('re', $app->request->get())) {
                $redirectPage = $app->request->get('re');
            }

            if (array_key_exists('invalid', $app->request->get())) {
                $invalid = true;
            }
            $app->render('userlogin.php', array('redirectPage' => $redirectPage, "invalid" => $invalid));
        } else {
            $app->redirect("./");
        }

    } else if ($pageid == 'newevent') {

        $eventTypeCtrl = new \App\EventTypeCtrl();
        // print_r($eventTypeCtrl);

        $user = null;
        if (array_key_exists('user', $_SESSION)) {
            $user = $_SESSION['user'];

        }
        if (!empty($user) || is_array($user)) {
            try {
                $eventTypes = $eventTypeCtrl->getAll();
            } catch (Exception $e) {
                print_r($e);
            }

            $app->render('newevent.php', array("eventTypes" => $eventTypes));
        } else {
            $app->redirect("/userauth?re=/newevent");
        }


    } else if ($pageid == 'userauthnew') {

        if (!array_key_exists("userid", $_SESSION)) {
            $app->render('userregister.php');
        } else {
            $app->redirect("./");
        }


    } else if ($pageid == 'confirmemail') {
        $app->render('confirm.php');


    } else if ($pageid == "thanksconfrimmail") {
        $username = $app->request->get("username");
        $confirm = $app->request->get("confirm");

        $userCtrl = new \App\UserCtrl();
        $confirmMessage = $userCtrl->confirmEmail($username, $confirm);
        $app->render('thanksconfrimmail.php');
        if ($confirmMessage) {
            $app->render('thanksconfrimmail.php');
        } else {
            messageView($app, "Email Confirmation Error",
                "Please check again or  <a style='color: #e1c7ff' href='./resendconfirmemail'>Resend confirmation code</a>", "danger");
        }

    } else if ($pageid == "resendconfirmemail") {


    } else if ($pageid == "resetpasswordcomplete") {
        $app->render('resetpasswordcomplete.php');


    } else if ($pageid == "resetpassword") {
        $app->render('resendpassword.php');


    } else if ($pageid == 'profile') {

        $user = null;
        if (array_key_exists('user', $_SESSION)) {
            $user = $_SESSION['user'];

        }
        if (!empty($user) || is_array($user)) {
            $app->render('profile.php');
        } else {
            $app->redirect("/");
        }


    } else if ($pageid == "accountsettings") {

        $user = null;
        if (array_key_exists('user', $_SESSION)) {
            $user = $_SESSION['user'];

        }
        if (!empty($user) || is_array($user)) {
            $invalidPassword = $app->request->get("invalidpassword");
            if (empty($invalidPassword)) {
                $invalidPassword = false;
            }

            $app->render('accountsettings.php', array("user" => $_SESSION['user'], "invalidpassword" => $invalidPassword));
        } else {
            $app->redirect("/");
        }


    } else {


        $tripctrl = new App\TripCtrl();
        $userctrl = new App\UserCtrl();

        $event = json_decode($tripctrl->getEvent($pageid));
        $user = ($userctrl->byUserID($pageid));

        if ($event) {


            $get = $app->request->get();
            if (array_key_exists('settings', $get)) {
                $app->render('event_edit.php', array('pageid' => $pageid, 'event' => $event, "settings" => $get['settings']));
            } else {
                $app->render('event.php', array('pageid' => $pageid, 'event' => $event));
            }


        } else if ($user) {
            $app->render('user.php', array('pageid' => $pageid, 'user' => $user));
        }


    }

});

$app->get('/dewmal/:pageid', function ($pageid) use ($app) {
    echo $pageid;
});
$app->get('/', function ($pageid) use ($app) {
    $app->render('event.php', array('pageid' => $pageid));
});


function messageView($app, $title = 'Title', $message = 'content', $type = 'default')
{
    $app->render('messages.php', array('title' => $title, 'message' => $message, 'type' => $type));
}

?>

