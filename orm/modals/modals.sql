CREATE TABLE IF NOT EXISTS `user` (
  `userid` varchar(20) NOT NULL,
  `username` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `confirm` varchar(255) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `fbid` varchar(255) DEFAULT NULL,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS trip (
  `tripid` varchar(200) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` longtext NOT NULL,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `coverphoto` text,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tripid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `location` (
  `locationid` varchar(40) NOT NULL,
  `lon` double(20,18) NOT NULL,
  `lat` double(20,18) NOT NULL,
  `description` text,
  `address` text NOT NULL,
  `visitdate` datetime DEFAULT NULL,
  `Trip` varchar(200) NOT NULL,
  PRIMARY KEY (`locationid`),
  KEY `fk_location_Trip1_idx` (`Trip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `photo` (
  `photoid` varchar(40) NOT NULL,
  `url` text NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `iscoverphoto` tinyint(1) NOT NULL DEFAULT '0',
  `location` varchar(40) DEFAULT NULL,
  `Trip` varchar(200) NOT NULL,
  PRIMARY KEY (`photoid`),
  KEY `fk_photo_location1_idx` (`location`),
  KEY `fk_photo_Trip1_idx` (`Trip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `trip_has_user` (
  `Trip` varchar(200) NOT NULL,
  `User` varchar(20) NOT NULL,
  `position` varchar(45) NOT NULL,
  PRIMARY KEY (`Trip`,`User`),
  KEY `fk_Trip_has_User_User1_idx` (`User`),
  KEY `fk_Trip_has_User_Trip_idx` (`Trip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;