<?php

namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class EntityCtrl {

    private $logger;
    protected $LOG = "UserCtrl =>";

    function __construct() {
        $this->logger = \Slim\Slim::getInstance()->getLog();
    }

    protected function loggLastQuery($connection) {
        $query= $connection->last_query;
        $this->logger->debug($this->LOG . $connection->last_query);
        return $query;
    }

    /**
     * Convert Result array to attribute results
     * @param type $activeResults
     * @return array
     */
    protected function convertTpResultArray($activeResults, array $options = array()) {
        $result = array();

        foreach ($activeResults as $key => $value) {
            array_push($result, $value->to_array($options));
        }
        return $result;
    }

    /**
     * Convert result to Json array
     * 
     * 
     * @param type $activeResults
     * @param array $options
     * @return type
     */
    protected function convertToJsonResultArray($activeResults, array $options = array()) {
        $result = array();

        foreach ($activeResults as $key => $value) {
            array_push($result, $value->to_array($options));
        }
        return json_encode($result);
    }

}
