<?php

namespace App;

use ActiveRecord;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class EventTypeCtrl extends EntityCtrl {

    protected $LOG = "TripCtrl =>";

    function __construct() {
        parent::__construct();
    }

    /**
     * Get Event Type Object by id
     * @param type $param
     * @return boolean
     */
    public function get($id) {
        $eventType = EventType::first(array(
            "conditions" => array(
                "id" => $id
            )
        ));

        if ($eventType) {
            return $eventType->attributes();
        } else {
            return FALSE;
        }
    }

    /**
     * Get all Event types from database
     * @return type
     */
    public function getAll() {
        $all = EventType::all();
        return $this->convertToJsonResultArray($all);
    }

}

class EventType extends ActiveRecord\Model {

    static $table_name = 'event_type';

}
