<?php

namespace App;

use ActiveRecord;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class NewUserCtrl extends EntityCtrl {

    protected $LOG = "TripCtrl =>";

    function __construct() {
        parent::__construct();
    }

    /**
     * Get new user from user id
     * 
     * @param type $userid
     * @return type
     */
    function getFromUserid($userid) {        
        $newuser = NewUser::first(array(
                    "conditions" => array("userid" => $userid)
        ));
        if ($newuser) {            
        } else {
            $newuser = new NewUser(array(
                "userid" => $userid
            ));
            $newuser->save();
        }
        //print_r($newuser);
        return $newuser->attributes();
    }

}

class NewUser extends \ActiveRecord\Model {

    static $table_name = 'newuser';

}
