<?php

namespace App;

use ActiveRecord;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PhotCtrl extends EntityCtrl
{

    protected $LOG = "PhotCtrl =>";

    function __construct()
    {
        parent::__construct();
    }


    /**
     * Get by photoid
     * @param type $photoid
     */
    function getById($photoid) {
        $photo=  Photo::first('all',array(            
            "conditions" => array("photoid ='$photoid'")
        ));
       // echo Photo::connection()->last_query;
        return $photo->attributes();
    }
    
    /**
     * 
     * get photos by event id
     * 
     * @param type $tripid
     * @param type $start
     * @param type $limit
     */
    function getPhotoByEvent($tripid, $start, $limit, $withcoverimage = false) {

        if ($withcoverimage) {

            $photos = Photo::find('all', array(
                        "conditions" => array("Trip ='$tripid'"),
                'limit' => $limit, 'offset' => $start,
                "order" => "photoid"
            ));
        } else {
            $photos = Photo::find('all', array(
                        "conditions" => array("Trip ='$tripid'","location is not null"),
                'limit' => $limit, 'offset' => $start,
                "order" => "photoid"
            ));
            
            //echo 
        }

        $result = array();
        foreach ($photos as $key => $value) {
            array_push($result, $value->attributes());
        }
       // array_push($result,Photo::connection()->last_query);
        return $result;
    }

    /**
     * Number of photos have to trip
     * @param type $tripid
     * @return type
     */
    function getPhotoCountByTripId($tripid) {
        //  echo $tripid;
        $count = Photo::first(
                        array(
                            'select' => ' count(*) AS num_rows',
                            'conditions' => array("Trip ='$tripid'")
                ))->attributes();
        //echo Photo::connection()->last_query;
        //   echo  $count['num_rows'];
        //  ;photo
        return $count;
    }

    /**
     * Save photo
     * @param $imageurl
     * @param $tripid
     * @return string
     */
    public function savePhoto($imageurl, $tripid,$user)
    {
        $photoid = unique_id(20);
        $newPhoto = new Photo(
            array(
                'photoid' => $photoid,
                'url' => $imageurl,
                'Trip' => $tripid,
                'user'=>$user
            )
        );
        $newPhoto->save();
        return $newPhoto->attributes();
    }

    public function updatePhoto($photo)
    {
        $photoObj = Photo::find($photo->photoid);
        $photoObj->update_attribute("description", $photo->description);

    }

    public function getRandom($int)
    {
        $join = "JOIN  trip as t ON(photo.Trip = t.tripid) AND t.published";
        $photos = Photo::all(array(
            "order" => "RAND()",
            "limit" => $int,
            "joins" => $join
        ));

        return $this->convertTpResultArray($photos);

    }


    /**
     *
     * All photos by event
     *
     * @param $tripid
     * @return array
     *
     */
    public function getPhotoByEventAll($tripid)
    {
        $photos = Photo::find('all', array(
            "conditions" => array("Trip ='$tripid'"),
            "select" => "url,description,title",
            "order" => "photoid"
        ));
        $result = array();
        foreach ($photos as $key => $value) {
            array_push($result, $value->attributes());
        }
        // array_push($result,Photo::connection()->last_query);
        return $result;
    }

}

class Photo extends ActiveRecord\Model {

    static $table_name = 'photo';

}
