<?php

namespace App;

use ActiveRecord;

//include_once '../modals/modals.php';


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LocationCtrl {

    function __construct() {
        
    }


    /**
     * Save location
     *
     * @param $params
     * @return string
     */
    function saveLocation($params){

        if(array_key_exists('locationid',$params)){
            $location=Location::first(array(
                "conditions"=>array(
                    "locationid"=>$params['locationid']
                )
            ));
            $location->set_attributes($params);
            $location->save();
            return "update";
        }else{
            $params['locationid']=unique_id(20);
            $location=new Location($params);
            $location->save();
            return "new";
        }

    }


    /**
     * Get location trip count
     * 
     * @param type $tripid
     * @return type
     */
    function getLocationCountByTrip($tripid) {

        //  echo $tripid;
        $count = Location::first(
                        array(
                            'select' => ' count(*) AS num_rows',
                            'conditions' => array("Trip ='$tripid'")
                ))->attributes();
        //echo Location::connection()->last_query;
        //   echo  $count['num_rows'];
        //  ;
        return $count['num_rows'];
    }

    /**
     * 
     * If user want delete location he should be the owner of the event
     * publishers cannot delete locations
     * 
     * @param type $locationid
     * @param type $userid
     * @return boolean
     */
    public function deleteLocation($locationid, $userid) {

        //echo $userid;
        // echo $locationid;

        $array = Location::find_by_sql(
                        "select l.locationid 
            from location as l Join trip_has_user as u 
ON ( (l.Trip=u.Trip) 
AND l.locationid='$locationid' 
AND u.`User`='$userid' AND u.`position`='OWNER' )"
        );


        if (count($array) == 1) {
            $locationid = '';
            foreach ($array as $value) {
                $locationid = $value->attributes()['locationid'];
                $locationid;
            }



            $potourls = Photo::all(array(
                        "conditions" => array(
                            "location" => $locationid
                        )
            ));
            $urls = array();
            foreach ($potourls as $photo) {
                $url = $photo->attributes()['url'];
                array_push($urls, $url);
            }

            //print_r($urls);



            Location::first(array(
                "conditions" => array(
                    "locationid" => $locationid
                )
            ))->delete();


           // $imageCtrl = new \Image();
          //  foreach ($urls as $url) {
           //     $imageCtrl->removeImage($url);
          //  }


            return TRUE;
        } else {
            return FALSE;
        }
    }

}

class Location extends ActiveRecord\Model {

    static $table_name = 'location';

}
