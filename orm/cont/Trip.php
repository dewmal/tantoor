<?php

namespace App;

use ActiveRecord;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TripCtrl extends EntityCtrl
{

    protected $LOG = "TripCtrl =>";

    function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * Update cover photo top
     *
     * @param type $tripid
     * @param type $top
     */
    function updateCoverPhotoTop($tripid, $top)
    {
        $trip = Trip::first(array(
            "conditions" => array(
                "tripid" => $tripid
            )
        ));
        $trip->update_attribute("coverphoto_top", $top);
    }

    function getCoverPhoto($tripid)
    {
        $trip = Trip::first(array(
            "conditions" => array(
                "tripid" => $tripid
            ),
            "select" => "coverphoto"
        ));
        $data = $trip->to_array();
        return $data['coverphoto'];
    }

    /**
     *
     * @param type $tripid
     */
    function tripUsers($tripid, $userid, array $positions = array("PUBLISHER", "VISITOR", "MEMBER", "ORGANIZER", "BLOCK"))
    {
        $join = "JOIN  trip_has_user as t ON(user.userid = t.User AND  user.userid <>'$userid') AND t.Trip='$tripid' ";
        $users = User::all(array(
            'select' => 'user.*,t.position',
            'conditions' => array("t.position IN (?)", $positions),
            'joins' => $join,
            'group' => 'username'));
        return $this->convertToJsonResultArray($users, array('except' => array('password', 'confirm')));
    }

    /**
     * Update trip object AS published
     * @param type $tripObj
     */
    function unpublishTrip($tripid, $userid, $type)
    {

        $updatedatetime = new \DateTime();
        $updatedatetimes = $updatedatetime->format('Y-m-d H:i');

        Trip::update_all(array(
            'set' => array('publish_type' => $type, "published" => FALSE,
                "updatedatetime" => $updatedatetimes
            ),
            'conditions' => array('tripid' => array($tripid)
            )));
        // echo Trip::connection()->last_query;
        $this->loggLastQuery(Trip::connection());

        $param = array(
            "userid" => $userid,
            "tripid" => $tripid,
            "name" => "Update",
            "description" => "unpublish event"
        );

        $updateHistoryCtrl = new UpdateHistoryCtrl();
        $updateHistoryCtrl->save($param);
    }

    /**
     * Update trip object AS published
     * @param type $tripObj
     */
    function publishTrip($tripid, $userid, $type)
    {

        $updatedatetime = new \DateTime();
        $updatedatetimes = $updatedatetime->format('Y-m-d H:i');

        Trip::update_all(array(
            'set' => array('publish_type' => $type, "published" => TRUE,
                "updatedatetime" => $updatedatetimes
            ),
            'conditions' => array('tripid' => array($tripid)
            )));
        // echo Trip::connection()->last_query;
        $this->loggLastQuery(Trip::connection());

        $param = array(
            "userid" => $userid,
            "tripid" => $tripid,
            "name" => "Update",
            "description" => "Publish event"
        );

        $updateHistoryCtrl = new UpdateHistoryCtrl();
        $updateHistoryCtrl->save($param);
    }

    /**
     * Update trip object
     * @param type $tripObj
     */
    function updateTrip($tripObj)
    {

        //print_r($tripObj);

        Trip::update_all(array(
            'set' => array('name' => $tripObj->name,
                'event_type' => $tripObj->event_type,
                'description' => $tripObj->description,
                'startdate' => $tripObj->startdate,
                'enddate' => $tripObj->enddate
            ),
            'conditions' => array('tripid' => array($tripObj->tripid)
            )));
        $this->loggLastQuery(Trip::connection());
    }

    /**
     *
     *
     * Get events by user
     *
     * @param type $userid
     * @param type $positions
     * @param type $limit
     * @param type $start
     * @param type $sorttype
     */
    public function getByUser($userid, $positions = array('PUBLISHER', 'OWNER'), $limit = 5000, $start = 0, $sorttype = "createtime")
    {
        $join = " join trip_has_user as tu ON (tu.Trip=trip.tripid AND "
            . "tu.User='$userid'  )";
        $events = Trip::all(array(
            'select' => "trip.*,tu.position,tu.User",
            'joins' => $join,
            'conditions' => array("trip.published is TRUE AND tu.position IN (?)", $positions),
            'limit' => $limit,
            'offset' => $start,
            'order' => "$sorttype DESC"
        ));

        return $this->convertToJsonResultArray($events);
    }


    /**
     *
     *
     * Get events by user
     *
     * @param type $userid
     * @param type $positions
     * @param type $limit
     * @param type $start
     * @param type $sorttype
     */
    public function getByOwner($userid, $positions = array('PUBLISHER', 'OWNER'), $limit = 5000, $start = 0, $sorttype = "createtime")
    {
        $join = " join trip_has_user as tu ON (tu.Trip=trip.tripid AND "
            . "tu.User='$userid'  )";
        $events = Trip::all(array(
            'select' => "trip.*,tu.position,tu.User",
            'joins' => $join,
            'conditions' => array("tu.position IN (?)", $positions),
            'limit' => $limit,
            'offset' => $start,
            'order' => "$sorttype DESC"
        ));

        return $this->convertToJsonResultArray($events);
    }

    /**
     *
     * Load Events by type with limit and start
     *
     * @param type $type
     * @param type $limit
     * @param type $start
     * @return type
     */
    public function getByTypeUser($type, $limit, $start, $userid, $sorttype = "createtime")
    {

        //echo $type." ".$limit." ".$sorttype;
        if ($type != 'ALL') {
            $conditions = array("event_type" => array($type));
        } else {
            $conditions = array();
        }

        $conditions['publish_type'] = 'PUBLIC';


        $conditions["published"] = 1;
        $users = Trip::find('all', array(
            'conditions' => $conditions,
            'limit' => $limit,
            'offset' => $start,
            'order' => "$sorttype DESC"
        ));

        //echo $this->loggLastQuery(Trip::connection());
        return $this->convertToJsonResultArray($users);
    }

    /**
     *
     * Load Events by type with limit and start
     *
     * @param type $type
     * @param type $limit
     * @param type $start
     * @return type
     */
    public function getByType($type, $limit, $start, $userid, $sorttype = "createtime")
    {

        //echo $type." ".$limit." ".$sorttype;
        if ($type != 'ALL') {
            $conditions = array("event_type" => array($type));
        } else {
            $conditions = array();
        }

        $conditions['publish_type'] = 'PUBLIC';


        $conditions["published"] = 1;
        $users = Trip::find('all', array(
            'conditions' => $conditions,
            'limit' => $limit,
            'offset' => $start,
            'order' => "$sorttype DESC"
        ));

        //echo $this->loggLastQuery(Trip::connection());
        return $this->convertToJsonResultArray($users);
    }

    public function getByUserWithme($userid, $myid, $positions = array('OWNER'), $limit = 5000, $start = 0, $sorttype = "createtime")
    {
        $join = " join trip_has_user as tu ON (tu.Trip=trip.tripid AND "
            . "(tu.User='$userid')  )";
        $events = Trip::all(array(
            'select' => "trip.*,tu.position,tu.User",
            'joins' => $join,
            'conditions' => array("trip.published is TRUE AND "
            . "tu.position IN (?)", $positions),
            'limit' => $limit,
            'offset' => $start,
            'order' => "$sorttype DESC"
        ));
    }

    /**
     * GEt event by id
     * @param $id
     */
    public function getEvent($id)
    {
        try {
            $join = " join trip_has_user as tu ON (tu.Trip=trip.tripid AND "
                . "(tu.position='OWNER')  )";
            $event = Trip::first(array(
                "select" => "trip.*,tu.User as owner",
                "conditions" => array("trip.tripid=?", $id),
                "joins" => $join

            ));

            if ($event) {
                return $event->to_json();
            }
        } catch (Exception $e) {
            print_r($e);
        }

    }


    /**
     * Update cover photo src
     * @param $url
     * @param $tripid
     */
    public function updateCoverPhotoSrc($url, $tripid)
    {
        $trip = Trip::first(array(
            "conditions" => array(
                "tripid" => $tripid
            )
        ));
        $trip->update_attribute("coverphoto", $url);
    }

    /**
     * Delete Trip
     *
     * @param $id
     */
    public function deleteEvent($id,$userid)
    {
        $event = Trip::first(array(
            "conditions" => array(
                "tripid" => $id
            )
        ));
        if ($event) {
            try {
                $event->delete();
            } catch (\Exception $e) {
                print_r($e);
            }
            return true;
        } else {
            return false;
        }
    }


    /**
     *
     * Add New event to database
     *
     * @param $trip
     *
     */
    public function newEvent($trip, $userid)
    {
        $tripid = unique_id(20);
        $newTrip = new Trip(
            array(
                'tripid' => $tripid,
                'name' => $trip['name'],
                'event_type' => $trip['type']
            )
        );
        $newTrip->save();


        $tripHasUser = new TripHasUser(
            array(

                "Trip" => $tripid,
                "user" => $userid,
                "position" => "OWNER"

            )
        );

        $tripHasUser->save();


        $param = array(
            "userid" => $userid,
            "tripid" => $tripid,
            "name" => "Create",
            "description" => "Create event"
        );

        $updateHistoryCtrl = new UpdateHistoryCtrl();
        $updateHistoryCtrl->save($param);

        return $tripid;

    }

}

class Trip extends ActiveRecord\Model
{

    static $table_name = 'trip';
    static $has_many = array(
        array(
            'trip_has_user'
        ),
        array('users', 'through' => 'trip_has_user')
    );

}
