<?php

namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class UserEventsEntityCtrl extends EntityCtrl {

    protected $LOG = "UserEventsEntityCtrl =>";

    /**
     * 
     * Update recode
     * 
     * @param type $tripid
     * @param type $userid
     * @param type $relation
     */
    function update($tripid, $userid, $relation) {

        $triphasuser = TripHasUser::first(
                        array(
                            'conditions' => array('User = ? AND Trip= ? AND position not in(?)',
                                $userid, $tripid, array('OWNER'))
        ));
        //  print_r($triphasuser->attributes());
        $triphasuser->update_attribute("position", $relation);






//        TripHasUser::update_all(array(
//            'set' => array('position' => $relation),
//            'conditions' =>
//            array('Trip' => array($tripid), 
//                "User" => array($userid),
//                'position not in'=>array("OWNER")
//        )));

        $this->loggLastQuery(TripHasUser::connection()); // echo UserHasEvents::connection()->last_query;
    }

    /**
     * 
     * Insert values
     * 
     * @param type $tripid
     * @param type $userid
     * @param type $relation
     * 
     */
    function save($tripid, $userid, $relation) {

        $triphasuser = TripHasUser::first(array(
                    'conditions' => array('Trip' => array($tripid), "User" => array($userid)
        )));



        if (is_null($triphasuser) || !$triphasuser->is_new_record()) {
            TripHasUser::create(
                    array("Trip" => $tripid, "User" => $userid, "position" => $relation)
            );
            $this->loggLastQuery(TripHasUser::connection()); // echo UserHasEvents::connection()->last_query;
        } else {
            
        }
    }

    /**
     * Delete from DB
     * 
     * @param type $tripid
     * @param type $userid
     */
    function delete($tripid, $userid) {
        $all = TripHasUser::all(
                        array(
                            "conditions" => "Trip='$tripid' AND User='$userid'"
                        )
        );
        foreach ($all as $object) {
            $object->delete();
        }
        $this->loggLastQuery(TripHasUser::connection()); // echo UserHasEvents::connection()->last_query;
    }

    public function getAllUsersByTrip($tripid) {

        $join = "Join user as u ON (u.userid=trip_has_user.User AND trip_has_user.Trip='$tripid')";

        $all = TripHasUser::all(
                        array(
                            "select"=>"*",
                            "joins" => $join
                        )
        );

        return $this->convertToJsonResultArray($all, array('except' => array('password', 'confirm')));
    }

}

class TripHasUser extends \ActiveRecord\Model {

    static $table_name = 'trip_has_user';
    static $belongs_to = array(
        array(
            'trip', # The table name it's linked to
            'foreign_key' => 'Trip', # key in linked (this) table
            'primary_key' => 'tripid', # key in "parent" table
        ),
        array('user', 'foreign_key' => 'User')
    );

}
