<?php

namespace App;

use ActiveRecord;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LikeCtrl extends EntityCtrl {

    protected $LOG = "LikeCtrl =>";

    function __construct() {
        parent::__construct();
    }

    /**
     * 
     * @param type $tripid
     * @param type $userid
     * @return boolean
     */
    function delete($tripid, $userid) {
        $like = Like::first(array(
                    "conditions" => array(
                        "tripid" => $tripid,
                        "userid" => $userid
        )));
        ($this->loggLastQuery(Like::connection()));
        if ($like) {
            $like->delete();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * 
     * @param type $tripid
     * @param type $userid
     * @return boolean
     */
    function save($tripid, $userid) {

        $like = Like::first(array(
                    "conditions" => array(
                        "tripid" => $tripid,
                        "userid" => $userid
        )));



        if (!$like) {
            $like = new Like(array(
                "tripid" => $tripid,
                "userid" => $userid));
            $like->save();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getUserLikesFor($id) {
        $join = "JOIN trip as t ON(t.tripid = likes.tripid AND likes.userid ='$id')";
        $activeResults = Like::all(array(
                    'select' => '*',
                    'joins' => $join,
        ));
        return $this->convertToJsonResultArray($activeResults, array("password", "confirmid"));
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function getLikesFormTrip($id) {
        $join = "JOIN user as u ON ((likes.userid = u.userid )AND likes.tripid='$id')";
        $activeResults = Like::all(array(
                    'select' => '*',
                    'joins' => $join,
        ));
        return $this->convertToJsonResultArray($activeResults);
    }

    /**
     * find user like
     * 
     * @param type $tripid
     * @param type $userid
     * @return boolean
     */
    public function getUserLikeOrNot($tripid, $userid) {
        $like = Like::first(array(
                    "conditions" => array(
                        "tripid" => $tripid,
                        "userid" => $userid
        )));

        if ($like) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getLikeCount($tripid) {
        $activeResults = Like::first(array(
                    'select' => 'count(*) as count',
                    "conditions" => array(
                        "tripid" => $tripid)
        ));
        return ($activeResults->attributes());
    }

}

class Like extends ActiveRecord\Model {

    static $table_name = 'likes';

}
