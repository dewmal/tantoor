<?php

namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class UserCtrl extends EntityCtrl {

    protected $LOG = "UserCtrl =>";

    function getAllTrips($userid, array $positions = array("PUBLISHER", "VISITOR", "MEMBER", "ORGANIZER", "BLOCK")) {



        $join = "JOIN  trip_has_user as t ON(trip.tripid = t.Trip) AND t.User='$userid'";
        $users = Trip::all(array(
                    'select' => 'trip.*,t.position',
                    'conditions' => array("t.position IN (?)", $positions),
                    'joins' => $join,
                    'group' => 'tripid'));

        return $this->convertToJsonResultArray($users);
    }

    /**
     * Find user by username firstname
     * @param type $param
     * @param type $limit
     * @return type
     */
    function findByAnything($param, $limit, $userid) {

        $users = User::find('all', array(
                    'select' => 'userid,username, email,fbid,type,firstname,createtime,propic',
                    'conditions' => array(" userid not in ( '$userid') AND (username LIKE '%$param%' OR firstname LIKE '%$param%') ")
                    ,
                    'limit' => $limit,
                    'order' => "firstname"
        ));
        $this->loggLastQuery(User::connection()); // echo UserHasEvents::connection()->last_query;
        return $this->convertTpResultArray($users);
    }

    /**
     * 
     * Select users randomly
     * 
     * @param type $limit
     * @return array
     */
    function getRandomUsers($limit) {

        $users = User::find("all", array(
                    'select' => 'userid, username, email, fbid, type, firstname, createtime,propic'
                    ,
                    "order" => "rand() ",
                    "limit" => $limit));


        $this->loggLastQuery(User::connection()); // echo UserHasEvents::connection()->last_query;

        return $this->convertTpResultArray($users);
    }

    /**
     * get by userid
     * 
     * @param type $userid
     * @return type
     */
    function byUserID($userid) {
        $users = User::first('all', array(
                    'select' => 'userid, username, email, fbid, type, firstname, createtime,propic'
                    ,
                    "conditions" => array("userid" => $userid)
        ));
        $this->loggLastQuery(User::connection());

        //log("asdasd");
        if ($users) {
            return ($users->attributes());
        }
    }

    /**
     * get by username
     * 
     * @param type $username
     * @return type
     */
    function byUserName($username) {
        $users = User::first('all', array(
                    'select' => 'userid, username, email, fbid, type, firstname, createtime'
                    ,
                    "conditions" => array("username" => $username)
        ));
        $this->loggLastQuery(User::connection());

        //log("asdasd");
        if ($users) {
            return ($users->attributes());
        }
    }

    /**
     * If there are no user then return null
     * 
     * 
     * @param type $email
     * @return type
     */
    public function byEmail($email) {
        $users = User::first('all', array(
                    'select' => 'userid, username, email, fbid, type, firstname, createtime'
                    ,
                    "conditions" => array("email" => $email)
        ));
        $this->loggLastQuery(User::connection());

        //log("asdasd");
        if ($users) {
            return ($users->attributes());
        }
    }

    /**
     *
     * Get user details from username and password
     *
     * @param $username
     * @param $password
     * @return string
     */
    public function isValidLoginDetails($username, $password)
    {
        $password = passwordHash($password);


        $user = User::first('all', array(
            'select' => 'userid, username, email, fbid, type, firstname, createtime'
        ,
            "conditions" => array("username" => $username, "password" => $password)
        ));

        print_r($user);

        if (!empty($user)) {
            return $user->to_array();
        } else {
            return empty($user);
        }
    }

    public function registerNewUser($userDetails)
    {

        $userid = unique_id(20);
        $confirm = unique_id(30);

        $user = new User(
            array(
                "userid" => $userid,
                "confirm" => $confirm,
                "username" => $userDetails->email,
                "firstname" => $userDetails->firstname,
                "email" => $userDetails->email,
                "password" => passwordHash($userDetails->password),
                "gender" => $userDetails->gender
            )
        );
        $user->save();

        return $user->to_json();

    }

    public function confirmEmail($username, $confirm)
    {
        $user = User::first('all', array(
            'select' => 'userid, confirm'        ,
            "conditions" => array("username" => $username)
        ));

        if ($user) {
            $userdetails=$user->to_array();
            if($userdetails["confirm"]==$confirm){
                $user->update_attribute("confirm", null);
            }
            return TRUE;
        }else{
            return FALSE;
        }

    }


    /**
     * Assign new password
     *
     * @param $post
     */
    public function resetPassword($username)
    {


        $newpassword = unique_id(8);

        $user = User::first('all', array(
            'select' => 'userid,username, email,password',
            "conditions" => array("username" => $username)
        ));

        if ($user) {

            $user->update_attribute("password", passwordHash($newpassword));

            $userArray = $user->to_array();
            $userArray['password'] = $newpassword;

            return $userArray;


        } else {
            return FALSE;
        }

    }

    public function updateProfileDetails($username, $name, $value)
    {

        try {
            $user = User::first('all', array(
                'select' => '*',
                "conditions" => "username=" . $username . " OR email=" . $username
            ));

        if ($name == "password") {
            $value = passwordHash($value);
        }


        $user->update_attribute($name, $value);
        } catch (\Exception $e) {
            print_r($e);
        }
    }

}

class User extends \ActiveRecord\Model {

    static $table_name = 'user';
    static $has_many = array(
        array('trip_has_user', 'class_name' => "TripHasUser")
    );

}

function passwordHash($password)
{
    $password = md5($password);
    return $password;
}