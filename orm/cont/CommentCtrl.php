<?php

namespace App;

use ActiveRecord;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CommentCtrl extends EntityCtrl {

    protected $LOG = "CommentCtrl =>";

    function __construct() {
        parent::__construct();
    }

    /**
     * 
     * @param type $tripid
     * @return type
     */
    public function getEventComments($tripid, $limit = 10, $start = 0) {

        $join = "Join user as u ON(u.userid=comments.userid AND comments.tripid='$tripid')";

        $activeResults = Comment::all(array(
                    'select' => 'comments.*,u.firstname,u.propic',
                    'joins' => $join,
                    'limit' => $limit,
                    'order' => 'comments.commenteddatetime DESC',
                    'offset' => $start));

        return $this->convertToJsonResultArray($activeResults);
    }

    /**
     * 
     * @param type $userid
     * @param type $tripid
     * @param \App\Comment $comment
     */
    public function saveComment($userid, $tripid, $comment) {
        $comment = new Comment(array(
            "commentid" => unique_id(30),
            "userid" => $userid,
            "tripid" => $tripid,
            "comment" => $comment
        ));
        $comment->save();

        // return $comment->attributes();
        return $this->getComment($tripid, $userid);
    }

    /**
     * 
     * @param type $tripid
     * @return type
     */
    public function getComment($tripid, $userid, $limit = 1, $start = 0) {

        $join = "Join user as u ON((u.userid=comments.userid) AND (u.userid='$userid') AND  (comments.tripid='$tripid'))";

        $activeResults = Comment::first(array(
                    'select' => 'comments.*,u.firstname,u.propic',
                    'joins' => $join,
                    'limit' => $limit,
                    'order' => 'comments.commenteddatetime DESC',
                    'offset' => $start));

        return $activeResults->to_json();
    }

}

class Comment extends ActiveRecord\Model {

    static $table_name = 'comments';

}
