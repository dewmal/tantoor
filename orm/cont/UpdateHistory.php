<?php

namespace App;

use ActiveRecord;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class UpdateHistoryCtrl extends EntityCtrl {

    protected $LOG = "TripCtrl =>";

    function __construct() {
        parent::__construct();
    }

    /**
     * Get update history
     * 
     * @param type $tripid
     * @param type $userid
     * @return type
     */
    function getUpdateHistory($tripid, $userid, $limit = 1, $start = 0) {
        $join = "JOIN  updatehistory as ud "
                . "ON(user.userid = ud.userid"
                . " AND ud.tripid='$tripid')";

       // echo $userid;
        if ($userid) {
            $join = "JOIN  updatehistory as ud "
                    . "ON((user.userid = ud.userid) "
                    . "AND (ud.tripid='$tripid') "
                    . "AND (ud.userid='$userid'))";
        }

        $users = User::all(array(
                    'select' => '*',
                    'joins' => $join,
                    'group' => 'updatedatetime',
                    'order' => 'updatedatetime DESC',
                    'limit' => $limit,
                    'offset' => $start));


      // echo $this->loggLastQuery(User::connection());

        return $this->convertToJsonResultArray($users, array('except' => array('password', 'confirm')));
    }

    /**
     * Save 
     * @param type $param
     */
    public function save($param) {
        $updateHistory = new UpdateHistory($param);
        $updateHistory->save();
        $this->loggLastQuery(UpdateHistory::connection());
    }

}

class UpdateHistory extends ActiveRecord\Model {

    static $table_name = 'updatehistory';

}
