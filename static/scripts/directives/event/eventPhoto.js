// Generated by CoffeeScript 1.7.1
define(["./../module"], function(app) {
  return app.directive("eventPhoto", function() {
    var elementTop;
    elementTop = {};
    return {
      templateUrl: './static/partials/directives/event/eventphoto.html',
      scope: {
        photo: '=photo',
        editable: '=editable',
        onlastphoto: '=onlastphoto',
        onremove: '&onremove'
      },
      restrict: "EA",
      replace: false,
      link: function(scope, element, attr) {
        scope.element = element;
        if (scope.$last) {
          return scope.$apply(attr.onlastphoto);
        }
      },
      controller: function($scope, photoService) {
        $scope.updatePhotoDetails = function() {
          var promise;
          promise = photoService.update($scope.photo);
          promise.then(function(data) {
            return console.log(data);
          });
          return promise;
        };
        return $scope.deletePhotoDetails = function() {
          var promise;
          promise = photoService.update($scope.photo);
          promise.then(function(data) {
            $($scope.element).fadeOut(500);
            return $scope.onremove($scope.$index);
          });
          return promise;
        };
      }
    };
  });
});
