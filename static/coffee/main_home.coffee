debug = true

console.log(window.location)
locationHost = "http://"+window.location.host
scriptLocation = locationHost + '/static/scripts/'

require.config
  paths:
    jquery: scriptLocation + '../vendor/jquery/jquery.min'
    bootstrap: scriptLocation + '../vendor/bootflat/js/bootstrap.min'
    angular: scriptLocation + "../vendor/angularjs/angular/angular"
    'angular-route': scriptLocation + "../vendor/angularjs/angular/angular-route"
    'angular-upload': scriptLocation + "../vendor/angularjs/angular/angular-upload"
    domReady: scriptLocation + "../vendor/requirejs/domReady"
    mosaic: scriptLocation + "../vendor/mosaic/js/mosaic"
    qtip2: scriptLocation + "../vendor/qtip2/jquery.qtip.min"
    scripts: scriptLocation + "./scripts"
    imagesLoaded: scriptLocation + "../vendor/jquery/jquery.imagesloaded"
    select2:"../vendors/select2/select2.min"


  shim:
    jquery:
      exports: 'jquery'

    bootstrap:
      deps: ['jquery']

    mosaic:
      deps: ['jquery']

    qtip2:
      deps: ['jquery']
      exports: "qtip2"

    imagesLoaded:
      deps: ['jquery']
      expotst: 'imagesLoaded'


    select2:
      exports:"select2"
      deps:['jquery']


    scripts:
      deps: ["jquery", "qtip2"]


    angular:
      deps: ['jquery',"select2"]
      exports: "angular"




    "angular-route":
      deps: ["angular"]

    "angular-upload":
      deps: ["angular"]








  deps: [scriptLocation+"./bootstrapr.js"]


# require(["module/name", ...], function(params){ ... });
require [
  "bootstrap"
  "mosaic"
  "qtip2"
  "scripts"
  "select2"

], ($) ->
  return



