###
Defines the main routes in the application.
The routes you see here will be anchors '#/' unless specifically configured otherwise.
  asd
  asd
###

define ["./app"], (app) ->
  "use strict"

  app.config [
    "$routeProvider",'$locationProvider'
    ($routeProvider,$locationProvider) ->
      #$locationProvider.html5Mode(true);
      $routeProvider.when "/",
        templateUrl: "./static/partials/index.html"
        controller: "MyCtrl2"

      $routeProvider.when "/signin",
        templateUrl: "./static/partials/signin.html"

      $routeProvider.when "/event",
        templateUrl: "./static/partials/event.html"


      $routeProvider.otherwise redirectTo: "/"
  ]
