debug = true

require.config
  paths:
    jquery: '../vendor/jquery/jquery.min'
    bootstrap: '../vendor/bootflat/js/bootstrap.min'
    angular: "../vendor/angularjs/angular/angular"
    'angular-route': "../vendor/angularjs/angular/angular-route"
    'angular-upload': "../vendor/angularjs/angular/angular-upload"
    domReady: "../vendor/requirejs/domReady"
    scripts: "./scripts"
    vegas:'../vendor/vegas/jquery.vegas.min'
    select2:"../vendors/select2/select2.min"


  shim:
    jquery:
      exports: 'jquery'

    bootstrap:
      deps: ['jquery']

    vegas:
      exports:'vegas'
      deps:['jquery']


    scripts:
      deps: ["jquery"]

    select2:
      exports:"select2"
      deps:['jquery']


    angular:
      deps: ['jquery' ,"select2"]
      exports: "angular"


    "angular-route":
      deps: ["angular"]

    "angular-upload":
      deps: ["angular"]



  deps: ["./bootstrapr"]


# require(["module/name", ...], function(params){ ... });
require [
  "bootstrap"
  "scripts"
  "vegas"
  "select2"
], ($) ->



  return



