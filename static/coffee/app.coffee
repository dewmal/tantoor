###
loads sub modules and wraps them up into the main module
this should be used for top-level module definitions only
###
console.log 'dewmal'
define [
  "angular"
  "angular-route"
  "angular-upload"
  "./directives/index"
  "./controllers/index"
  "./factory/index"
  "./filters/index"
  "./uicontrollers/select2"

], (angular) ->
  "use strict"
  angular.module "app", [
    "ui.select2"
    "app.controllers"
    "app.directives"
    "app.factories"
    "app.filters"
    "ngRoute"
    "angularFileUpload"


  ]
