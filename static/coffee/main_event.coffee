debug = true

require.config
  paths:
    jquery: '../vendor/jquery/jquery.min'
    bootstrap: '../vendor/bootflat/js/bootstrap.min'
    angular: "../vendor/angularjs/angular/angular"
    'angular-route': "../vendor/angularjs/angular/angular-route"
    'angular-upload': "../vendor/angularjs/angular/angular-upload"
    domReady: "../vendor/requirejs/domReady"
    mosaic: "../vendor/mosaic/js/mosaic"
    qtip2: "../vendor/qtip2/jquery.qtip.min"
    scripts: "./scripts"
#    wookmark: '../vendor/wookmark/jquery.wookmark.min'
    imagesLoaded: "../vendor/jquery/jquery.imagesloaded"
#    colorbox: "../vendor/wookmark/libs/jquery.colorbox-min"
    vegas:'../vendor/vegas/jquery.vegas.min'
    draggable_background: '../vendor/draggable_background/draggable_background'
    lightbox:"../vendor/lightbox/js/lightbox.min"
    select2:"../vendors/select2/select2.min"

  shim:
    jquery:
      exports: 'jquery'

    bootstrap:
      deps: ['jquery']

    mosaic:
      deps: ['jquery']

    qtip2:
      deps: ['jquery']
      exports: "qtip2"

    imagesLoaded:
      deps: ['jquery']
      expotst: 'imagesLoaded'

#    colorbox:
#      deps: ['jquery', 'imagesLoaded']
#      exports: 'colorbox'

#    wookmark:
#      exports: 'wookmark'
#      deps: ['jquery', "imagesLoaded", 'colorbox']

    select2:
      exports:"select2"
      deps:['jquery']

    vegas:
      exports:'vegas'
      deps:['jquery']

    draggable_background:
      exportts: 'draggable_background'
      deps: ['jquery']

    lightbox:
        exports:"lightbox"
        deps:["jquery"]



    scripts:
      deps: ["jquery", "qtip2"]


    angular:
      deps: ['jquery',"select2"]
      exports: "angular"




    "angular-route":
      deps: ["angular"]

    "angular-upload":
      deps: ["angular"]








  deps: ["./bootstrapr"]


# require(["module/name", ...], function(params){ ... });
require [
  "bootstrap"
  "mosaic"
  "qtip2"
  "scripts"
#  "wookmark"
  "select2"
  "lightbox"
  "vegas"
  "draggable_background"

], ($) ->



  return



