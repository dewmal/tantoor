define [
  "./dialog"
  "./event/eventComponentLiner"
  "./event/eventPage"
  "./event/addEvent"
  "./event/eventPhoto"
  "./event/uploadPhotos"
  "./event/ownercomponent"
  "./event/shareevent"

  "./user"
  "./uiController"
]