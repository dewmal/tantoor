console.log 'asd'
define ["./module"], (app)->
  app.directive "userDetails", ['$http', '$compile', '$rootScope', ($http, $compile, $rootScope)->
    userScope = $rootScope.$new();
    userScope.user = 'asd'
    restrict: "EA"
    scope:
      user: '=user'
    link: (scope, element, el)->
      $(element).qtip
        content:
          text: ->
            $http.get("./static/partials/directives/user/userLarge.html").then((response) ->
              userScope.user = scope.user;
              if(!userScope)
                userScope.user = {
                  firstname: 'Your name?'
                  propic: "./static/img/user_1.png"
                }

              # The then function here is an opportunity to modify the response
              content = $compile(response.data)(userScope);
              scope.$apply
              console.log content
              return content
              # The return value gets picked up by the then in the controller.
              #response.data
            )
        show:
          delay: 500
          solo: true
        style: "qtip-light qtip-bootstrap qtip-square"
        hide:
          fixed: true,
          delay: 400
        position:
          my: "top left"
          at: 'bottom left'
          viewport: $(document)
          adjust:
            method: 'none'
  ]


  app.directive "userComponentLg", ($http)->
    templateUrl: "./static/partials/directives/user/userLarge.html"
    restrict: "EA"
    replace: true

  app.directive "userLoginPromot", ($http)->
    templateUrl: "./static/partials/directives/user/userloginpromot.html"
    restrict: "EA"
    replace: false
    link: (scope, element, attr)->
      elem=$(element).find('#loginbtnprompt').attr( 'href', './userauth?re='+attr.userLoginPromot)
      return
    contoller: ($scope)->
      console.log $scope.$parent.event
      return


  app.directive "userComponentImg", ($http, $compile) ->
    templateUrl: "./static/partials/directives/user/Image.html"
    restrict: "EA"
    scope:
      user: "=user"



    controller: ($scope) ->
      if debug
        console.log 'asdas'
      return

