define ["./../module"], (app)->
  app.directive "ownerComponent", ()->
    templateUrl: './static/partials/directives/event/ownercomponent.html'
    restrict: "EA"
    replace: true
    scope:
      event: '=event'
    controller: ($scope, eventService)->
      if debug
        console.log $scope.event

      eventService.photoCount($scope.event.tripid).then((data)->
        $scope.event.photocount = data.num_rows
      )

      eventService.getLastUpdateUser($scope.event.tripid).then((data)->
        console.log data
        $scope.event.lastupdate = data[0];
      )


      $scope.unpublish = ()->
        eventService.unpublish($scope.event).then((data)->
          console.log data
          $scope.event.published = 0
          createGrowl("Event","Event was un published successfully ")
        )
        return

      $scope.publish = ()->
        eventService.publish($scope.event).then((data)->
          console.log data
          $scope.event.published = 1
          createGrowl("Event","Event was published successfully ")

        )
        return


      $scope.delete = ()->
        eventService.publish($scope.event).then((data)->
          console.log data
          $scope.event.published = 1
          createGrowl("Event","Event was deleted")
        )
        return


      $scope.deleteEvent = ()->
        eventService.delete($scope.event).then((data)->
          if(data is "1Valid access")
            delete $scope.event
            createGrowl("Event","Event was deleted")
        )
        return

      return


    link: (scope, element, attr)->
      return

