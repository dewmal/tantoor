define ["./../module"], (app)->
  app.directive 'uploadPhotos', ()->
    templateUrl: "./static//partials/directives/event/uploadPhoto.html"
    restrict: "EA"
    replace: true
    link: (scope, element, elm)->
      $closebtn = $(element).find('.close')
      $closebtn.click(
        ()->
          $(element).fadeOut ->
            $(element).remove();
          , 'slow'
      )
    #$(document).append($(element))
    #$(element).modal('show')
    controller: ($scope, $fileUploader, $rootScope, $compile)->

      $scope.fileUploadOptions=
        url: './request/photo/upload'
        id:'photo'

      uploader = $scope.uploader = $fileUploader.create(
        filters: [(item) -> # A user-defined filter
          console.log "filter1"
          console.log item
          true
        ])

      # ADDING FILTERS
      uploader.filters.push (item) -> # second user filter
        console.info "filter2"
        true

      uploader.bind "afteraddingfile", (event, item) ->
        if item.id==$scope.fileUploadOptions.id
          item.removeAfterUpload=true
          console.info "After adding a file", item
          item.formData = [
            {tripid: $scope.event.tripid}
          ]
          item.upload()
        return


      uploader.bind "progressall", (event, progress) ->
        $scope.uploadprogress = progress
        return

      uploader.bind "success", (event, xhr, item, response) ->
        if item.id==$scope.fileUploadOptions.id
          console.info "Success", xhr, item, response
          #console.log (JSON.parse(response))

          try
            template = angular.element('<event-photo  editable="true" class="panel col-md-3" style="margin: 0px;" photo="photo"></event-photo>')
            $photoScope = $rootScope.$new();
            $photoScope.photo = response
            console.log response
            compiled = $compile(template)($photoScope);
            $('#uploadedPhotos').prepend(compiled)
            $scope.uploadprogress = 0
          catch e
            console.log e

        return


      $scope.finish = ()->
        console.log $scope
        $scope.clearEditPanel();