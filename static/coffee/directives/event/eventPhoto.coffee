define ["./../module"], (app)->
  app.directive "eventPhoto", ()->
    elementTop = {}

    templateUrl: './static/partials/directives/event/eventphoto.html'
    scope:
      photo: '=photo'
      editable:'=editable'
      onlastphoto: '=onlastphoto'
      onremove:'&onremove'


    restrict: "EA"
    replace: false
    link: (scope, element, attr)->
      scope.element = element
      if(scope.$last)
        scope.$apply(attr.onlastphoto)

    controller: ($scope, photoService)->
      $scope.updatePhotoDetails = ()->
        promise = photoService.update $scope.photo
        promise.then((data)->
          console.log data
        )
        return promise
      $scope.deletePhotoDetails = ()->
        promise = photoService.update $scope.photo
        promise.then((data)->
          $($scope.element).fadeOut(500)
          $scope.onremove($scope.$index)
        )
        return promise




