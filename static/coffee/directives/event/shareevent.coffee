define ["./../module"], (app)->
  app.directive 'shareEvent', ()->
    restrict: "A"
    scope:
      event: '=event'

    link: (scope, element, elm)->
      $(element).click(()->
        scope.shareEvent()
      )

      return
    controller: ($scope, fbService)->
      $scope.shareEvent = ()->
        FB.ui
          method: "feed"
          name: $scope.event.name
          link: "http://tantoor.com/" + $scope.event.tripid
          picture: "http://tantoor.com/image/timthumb.php?src=" + $scope.event.coverphoto + "&w=1024&zc=1&q=100"
          caption: $scope.event.name
          description: $scope.event.description+" #tantoor #event"
        , (response) ->
          if response and response.post_id
            createGrowl("Facebook","Post was published.")
          else
            createGrowl("Facebook", "Post was not published.")
          return

      return

  app.directive 'inviteFriends', ()->
    restrict: "A"
    link: (scope, element, elm)->
      $(element).click(()->
        scope.inviteFriends()
      )

      return
    controller: ($scope, fbService)->
      $scope.inviteFriends = ()->
        FB.ui
          method: "apprequests"
          message: "Share your event with tantoor"
        , ->
          return
      return
