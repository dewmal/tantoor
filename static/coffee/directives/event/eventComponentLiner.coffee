define ["./../module"], (app)->
  app.directive "eventComponentLinear", ->
    templateUrl: "./static/partials/directives/event/eventComponentLinear.html"
    restrict: "EA"
    scope:
      event: "=event"

    link: (scope, element, el)->
      $(element).find(".bar").mosaic
        animation: "slide"
        speed: 500
        opacity: 0.45


    controller: ($scope, eventService) ->
      $scope.photos=[]
      console.log 'asdas'

      eventService.photoCount($scope.event.tripid).then((data)->
        $scope.event.photocount=data.num_rows
      )

      eventService.getLastUpdateUser($scope.event.tripid).then((data)->
        console.log data
        $scope.event.lastupdate=data[0];
      )

      eventService.getPhotos($scope.event.tripid, 7).then((data)->
        $scope.photos = data;
      )

      return



