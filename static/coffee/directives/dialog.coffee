define ["./module"], (app) ->
  app.factory "createDialog", [
    "$document"
    "$compile"
    "$rootScope"
    "$controller"
    "$timeout"
    ($document, $compile, $rootScope, $controller, $timeout) ->
      defaults =
        id: null
        template: null
        templateUrl: null
        title: "Default Title"
        backdrop: true
        success:
          label: "OK"
          fn: null

        cancel:
          label: "Close"
          fn: null

        controller: null #just like route controller declaration
        backdropClass: "modal-backdrop"
        backdropCancel: true
        footerTemplate: null
        modalClass: "modal"
        css:
          top: "100px"
          margin: "0 auto"

      body = $document.find("body")
      return Dialog = (templateUrl, options, passedInLocals) -> #optional

        # Handle arguments if optional template isn't provided.
        if angular.isObject(templateUrl)
          passedInLocals = options
          options = templateUrl
        else
          options.templateUrl = templateUrl
        options = angular.extend({}, defaults, options) #options defined in constructor
        key = undefined
        idAttr = (if options.id then " id=\"" + options.id + "\" " else "")
        defaultFooter = "<button class='btn btn-square' ng-click=\"$modalCancel()\">{{$modalCancelLabel}}</button>" + "<button class='btn btn-primary btn-square' ng-click=\"$modalSuccess()\">{{$modalSuccessLabel}}</button>"
        footerTemplate = "<div class=\"modal-footer\">" + (options.footerTemplate or defaultFooter) + "</div>"
        modalBody = (->
          if options.template
            if angular.isString(options.template)

              # Simple string template
              "<div class=\"modal-body\">" + options.template + "</div>"
            else

              # jQuery/JQlite wrapped object
              "<div class=\"modal-body\">" + options.template.html() + "</div>"
          else

            # Template url
            "<div class=\"modal-body\" ng-include=\"'" + options.templateUrl + "'\"></div>"
        )()

        #We don't have the scope we're gonna use yet, so just get a compile function for modal
        modalEl = angular.element("<div class=\"" + options.modalClass + " fade\"" + idAttr + " style=\"display: block;\">" + "  <div class=\"modal-dialog\">" + "    <div class=\"modal-content\">" + "      <div class=\"modal-header\">" + "        <button type=\"button\" class=\"close\" ng-click=\"$modalCancel()\">&times;</button>" + "        <h2>{{$title}}</h2>" + "      </div>" + modalBody + footerTemplate + "    </div>" + "  </div>" + "</div>")
        for key of options.css
          modalEl.css key, options.css[key]
        divHTML = "<div "
        divHTML += "ng-click=\"$modalCancel()\""  if options.backdropCancel
        divHTML += ">"
        backdropEl = angular.element(divHTML)
        backdropEl.addClass options.backdropClass
        backdropEl.addClass "fade in"
        handleEscPressed = (event) ->
          scope.$modalCancel()  if event.keyCode is 27
          return

        closeFn = ->
          body.unbind "keydown", handleEscPressed
          modalEl.remove()
          backdropEl.remove()  if options.backdrop
          return

        body.bind "keydown", handleEscPressed
        ctrl = undefined
        locals = undefined
        scope = options.scope or $rootScope.$new()
        scope.$title = options.title
        scope.$modalClose = closeFn
        scope.$modalCancel = ->
          callFn = options.cancel.fn or closeFn
          callFn.call this
          scope.$modalClose()
          return

        scope.$modalSuccess = ->
          callFn = options.success.fn or closeFn
          callFn.call this
          scope.$modalClose()
          return

        scope.$modalSuccessLabel = options.success.label
        scope.$modalCancelLabel = options.cancel.label
        if options.controller

          locals = angular.extend(
            $scope: scope
          , passedInLocals)
          ctrl = $controller(options.controller, locals)

          # Yes, ngControllerController is not a typo
          modalEl.contents().data "$ngControllerController", ctrl
        $compile(modalEl) scope
        $compile(backdropEl) scope
        body.append modalEl
        body.append backdropEl  if options.backdrop
        $timeout (->
          modalEl.addClass "in"
          return
        ), 200
        return
  ]