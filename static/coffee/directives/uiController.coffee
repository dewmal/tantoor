define ["./module"], (app)->
  app.directive "ngBlur", ->
    (scope, elem, attrs) ->
      elem.bind "blur", ->
        scope.$apply attrs.ngBlur
        return

      return

  app.directive "ngFocus", ($timeout) ->
    (scope, elem, attrs) ->
      scope.$watch attrs.ngFocus, (newval) ->
        if newval
          $timeout (->
            elem[0].focus()
            return
          ), 0, false
        return

      return


  app.directive "serverClick", ()->
    link: (scope, element, elm)->
      if debug
        console.log elm

      $(element).click((data)->

        status = $('<span class="fa fa-spinner fa-spin"></span>')
        $(this).append status
        promise = scope.$eval(elm.serverClick)
        promise.then((data)->

          status.delay(200).fadeOut(500)
        )

      )


  app.directive "ngThumb", [
    "$window"
    ($window) ->
      helper =
        support: !!($window.FileReader and $window.CanvasRenderingContext2D)
        isFile: (item) ->
          angular.isObject(item) and item instanceof $window.File

        isImage: (file) ->
          type = "|" + file.type.slice(file.type.lastIndexOf("/") + 1) + "|"
          "|jpg|png|jpeg|bmp|gif|".indexOf(type) isnt -1

      return (
        restrict: "A"
        template: "<canvas/>"
        link: (scope, element, attributes) ->
          onLoadFile = (event) ->
            img = new Image()
            img.onload = onLoadImage
            img.src = event.target.result
            return
          onLoadImage = ->
            width = params.width or @width / @height * params.height
            height = params.height or @height / @width * params.width
            canvas.attr
              width: width
              height: height

            canvas[0].getContext("2d").drawImage this, 0, 0, width, height
            return
          return  unless helper.support
          params = scope.$eval(attributes.ngThumb)
          return  unless helper.isFile(params.file)
          return  unless helper.isImage(params.file)
          canvas = element.find("canvas")
          reader = new FileReader()
          reader.onload = onLoadFile
          reader.readAsDataURL params.file
          return
      )
  ]

  app.directive "onlast", ->
    scope:
      onlast: '=onlast'
    link: (scope, element, attrs)->
      if(scope.$last)
        scope.$eval(scope.onlast)


  ###
Controller load function on scroll
  ###
  app.directive "whenScrolled", ->
    (scope, elm, attr) ->
      raw = elm[0]
      console.log "working"
      funCheckBounds = (evt) ->
        scope.$apply attr.whenScrolled  if document.documentElement.clientHeight + $(document).scrollTop() >= document.body.offsetHeight
        return

      $('body').scroll funCheckBounds
      return


  ###
  How to use nav - pills
  using nav - pills class select
  tab pane
  <ul class = "nav nav-pills nav-pills-square nav-stacked" >
  <li class = "link active" > < a > Upload photo < /a></ li >
  <li class = "link"
  tab = "uploadPhoto_tabtwo" > < a > From album < /a></ li >
  <
  /ul>
  ###
  app.directive 'navPills', ->
    restrict: 'C'
    link: (scope, element, elm)->
      lis = $(element).find('a')
      $.each(lis, (key, li)->
        $(li).click(->
          $(element).find('.active').removeClass('active')
          $(li).parent().addClass 'active'
        )
        return
      )


  ###
  How to use this

  <div class="col-md-12" style="min-width: 800px;">
   <div class="col-md-3">
       <ul class="nav nav-pills nav-pills-square nav-stacked" tab-pane="photoUploadTabPanel" >
           <li class="link active" tab="uploadPhoto_tabone"><a>Upload photo</ a > < /li>
           <li class="link" tab="uploadPhoto_tabtwo"><a>From album</ a > < /li>
       </ ul >
  <
  /div>
   <div class="col-md-9">
       <!-- Tab panes -->
       <div id="photoUploadTabPanel" class="tab-content">
           <div class="tab-pane active" id="uploadPhoto_tabone">Home </ div >
  <div class = "tab-pane"
  id = "uploadPhoto_tabtwo" > A < /div>
       </ div >

  <
  /div>
  </ div >
  ###
  app.directive 'tabPane', ->
    restrict: 'A'
    link: (scope, element, elm)->
      tabControllers = $(element).find('li[tab]');
      tabPanes = []
      $.each tabControllers, (key, controller)->
        $(controller).click ()->
          $('#' + elm.tabPane + ' > .active').removeClass('active')
          $('#' + $(this).attr('tab')).addClass('active')


