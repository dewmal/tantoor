define ["./../module"], (app)->
  app.controller "eventUpdate", ($scope, eventService, eventTypeService)->
    if debug
      console.log $scope




    $scope.event;
    eventService.getEvent($('*[eventid]').attr('eventid')).then((data)->
      $scope.event = data
      $scope.event_type = $scope.event.event_type
    )

    #$("#event_type_select").select2();



    $scope.eventTypes = []

    eventTypeService.all().then (data)->
      $scope.eventTypes = data


    $scope.load = ->
      console.log 'done'
    $scope.$watch('event_type', ()->
      if($scope.event_type == 'Other')
        $('#otherEvent').show();
      else
        $('#otherEvent').hide();
    )
    $scope.update = ()->
      event=angular.copy($scope.$parent.event)
      event.event_type=parseInt($scope.event_type)
      delete event.photos
      console.log event
      eventService.update(event).then((data)->
        console.log data
      )
