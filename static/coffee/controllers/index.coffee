###
attach controllers to this module
if you get 'unknown {x}Provider' errors from angular, be sure they are
properly referenced in one of the module dependencies in the array.
below, you can see we bring in our services and constants modules
which avails each controller of, for example, the `config` constants object.
###
  
define [
  "./ctrl1"
  "./ctrl2"

  "./event"
  "./event/updateEvent"
  "./event/newEvent"
  "./event/EventSettingsCtrl"
  "./event/EventmemberCtrl"

  "./user/userProfile"
  "./user/ownerProfile"
  "./authentication"

], ->
