define ["./module"], (controllers) ->
  "use strict"

  ###
  Selected event controll by this function
###

  controllers.controller "EventCtrl",
    ["$scope" , 'eventService', 'eventTypeService' , '$fileUploader', '$rootScope', '$compile', 'staticFile',
     'userAuthenticationService',
      ($scope, eventService, eventTypeService, $fileUploader, $rootScope, $compile, staticFile, userAuthenticationService)->
        $scope.curruntUser;



        ###
        Get Event by event id
        ###
        $scope.event;
        eventService.getEvent($('*[eventid]').attr('eventid')).then((data)->
          $scope.event = data
          $scope.event.photos = []
          $scope.event.photos1 = []
          $scope.event.photos2 = []

          userAuthenticationService.curruntUser().then((data)->
            $scope.curruntUser = data
            if $scope.event.owner is $scope.curruntUser.userid then $scope.isowner = true
          )


          $scope.event.beforeupload = []
          start = 0

          $scope.loadPhotos = ()->
            eventService.getPhotos($scope.event.tripid, start, 12).then((data)->
              i = 0

              for photo in data

                if i % 3 == 0
                  $scope.event.photos.push(photo)
                if i % 3 == 1
                  $scope.event.photos1.push(photo)
                if i % 3 == 2
                  $scope.event.photos2.push(photo)

                i++
            )
            start += 12

          $scope.loadPhotos()

          eventTypeService.convertToObject($scope.event.event_type).then((data)->
            $scope.event.event_type = data
          )
        )

        ###
        Upload Cover photo to server
        ###
        $scope.fileUploadOptionsCoverPhoto =
          url: './request/photo/coverphotoupload'
          id: 'coverphoto'

        uploader = $scope.uploader = $fileUploader.create(
          filters: [(item) -> # A user-defined filter
            console.log "filter1"
            console.log item
            true
          ])

        uploader.bind "afteraddingfile", (event, item) ->
          if item.id == $scope.fileUploadOptionsCoverPhoto.id
            console.info "After adding a file", item
            item.formData = [
              {tripid: $scope.event.tripid}
            ]
            item.upload()
          return

        uploader.bind "progress", (event, item, progress) ->
          console.info "Progress: " + progress, item
          return

        uploader.bind "success", (event, xhr, item, response) ->
          if item.id == $scope.fileUploadOptionsCoverPhoto.id
            console.info "Success Coverphoto", xhr, item, response
            $scope.event.coverphoto = response.url
            createGrowl("Cover photo","Successfully added cover photo")
        #console.log (JSON.parse(response))

        ###
        Upload Cover photo to server script end
        ###


        ###

        ###
        $scope.changeBackGroundPositon = ->
          coverPhoto = $('#eventCoverPhoto')
          coverPhotoClone = coverPhoto.clone()

          messageBoard = coverPhoto.find('#messageBoard').show('slow')
          button = messageBoard.find('button')
          button.click((data)->
            messageBoard.hide('slow')
          )

          coverPhoto.css('cursor', 'crosshair')
          coverPhoto.backgroundDraggable({
            bound: true,
            axis: 'y',
            onStart: (x, y)->
              button.show()
            onStop: (x, y)->
              $scope.updateCoverImagePosition(x, y)
          })


        $scope.updateCoverImagePosition = (x, y)->
          eventService.updateCoverImagePosition($scope.event.tripid, y).then((data)->
          )


        $('#eventViewPage').show(1, ()->

        )


        options =
          autoResize: true # This will auto-update the layout when the browser window is resized.
          container: $("#main") # Optional, used for some extra CSS styling
          offset: 20 # Optional, the distance between grid items
          flexibleWidth: '50%'
          itemWidth: 230 # Optional, the width of a grid item

        $scope.trigerOnDelete = (photo)->
          $scope.event.photos.splice(photo, 1);



        ##Clear edit panel Container
        $scope.clearEditPanel = ->
          $('#control-panel').hide('slow')
          $('#control-panel').html('');


        $scope.showImageUploadView = ->
          userAuthenticationService.curruntUser().then((data)->
            if data.userid
              dialog = $compile($('<upload-photos></upload-photos>'))($scope)
              dialog = $(dialog)
              controlpanle = $('#control-panel')
              controlpanle.show(200)
              controlpanle.append(dialog)
              dialog.scrollTo()
            else
              dialog = '<div user-login-promot="' + $scope.event.tripid + '"></div>';
              console.log dialog
              dialog = $compile($(dialog))($scope)
              #dialog = $(dialog)
              console.log dialog
              dialogue(
                dialog,
                'Sign in  Required')
          )
          return


        $scope.updateEventDetails = ->
          $scope.updateMode = true
          staticFile.get('./static/partials/directives/event/eventUpdate.html').then((data)->
            updateView = $compile($(data))($scope)
            updateView = $(updateView)
            controlpanle = $('#control-panel')
            controlpanle.show(200)
            controlpanle.html(updateView)
          )
          return


        console.log $scope


        $scope.unpublish = ()->
          eventService.unpublish($scope.event).then((data)->
            console.log data
            $scope.event.published = 0
            createGrowl("Event","Event was un published successfully ")
          )
          return


        $scope.publish = ()->
          eventService.publish($scope.event).then((data)->
            console.log data
            $scope.event.published = 1
            createGrowl("Event","Event was published successfully ")

          )
          return


    ]


  ###
  Event List Controll by this controller class
###
  controllers.controller "EventViewCtrl", ['$scope', 'eventListService', ($scope, eventListService) ->
    $scope.events = []

    counter = 0
    limit = 5
    eventListService.subcribe (data, refresh) ->
      console.log data
      $scope.loadingGif = false
      if debug
        console.log data, refresh
      if refresh
        if debug
          console.log data
        $scope.events = data

        # $scope.$apply();
      else
        i = 0

        while i < data.length
          trip = data[i]

          #console.log(i);
          $scope.events.push trip
          i++

      if debug
        console.log data
      return

    eventListService.setvalues counter, limit
    $scope.loadMore = ->
      console.log 'start'
      $scope.loadingGif = true
      eventListService.loadMore()
      return

    $scope.loadMore()

    $scope.startUploadPhoto = ()->
      if debug
        console.log 'AB'
  ]

