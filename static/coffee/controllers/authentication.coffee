define ["./module"], (app) ->
  'use strict'
  app.controller 'loginCtrl', ($scope, userAuthenticationService, $location)->
    $('#loginPanel').show(50)
    bgSlideShow() ##Background image slide show
    $scope.user = {
      valid: true
    }
    $scope.loginAction = ->
      userAuthenticationService.login($scope.user.username, $scope.user.password).then((data)->
        $scope.valid = data
        if data.valid
          window.location = $('#redirectPageLocation').val();
        else
          $scope.user = {}

      )


  app.controller 'registerCtrl', ($scope, userAuthenticationService)->
    $('#registerPanel').show(50)
    bgSlideShow() ##Background image slide show
    $scope.user = {
      gender: 'male'
      error: {}
      valid: {}
    }

    $scope.invalid = true

    $scope.validatePassword = ->
      password = $scope.user.password
      if password
        $scope.user.error.password = false
        $scope.user.valid.password = true
      else
        $scope.user.error.password = true
        $scope.user.valid.password = false

      $scope.validte()

    $scope.validateFirstName = ->
      firstname = $scope.user.firstname
      if firstname
        $scope.user.error.firstname = false
        $scope.user.valid.firstname = true
      else
        $scope.user.error.firstname = true
        $scope.user.valid.firstname = false
      $scope.validte()

    $scope.validateEmail = ->
      email = $scope.user.email
      filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/
      valid = String(email).search(filter) isnt -1
      if valid
        userAuthenticationService.validemail(email).then((data)->
          data = JSON.parse(data)
          if debug
            console.log data
          $scope.user.error.email = data
          $scope.user.valid.email = !data
        )
      else
        $scope.user.error.email = true
        $scope.user.valid.email = false

      $scope.validte()

      return


    $scope.validte = ()->
      error = $scope.user.error
      $scope.invalid = error.email && error.username && error.firstname && error.password
      if !$scope.invalid then $scope.invalid = false
      console.log $scope.invalid
      return


    $scope.registerUser = ()->
      error = $scope.user.error

      $scope.validateEmail()

      $scope.validateUsername()

      $scope.validatePassword()

      $scope.validateFirstName()

      if !(error.email && error.username && error.firstname && error.password)
        userAuthenticationService.register($scope.user).then((data)->
          console.log data
          $scope.user = {
            gender: 'male'
            error: {}
            valid: {}
          }

          #window.location.href='./confirmemail'
        )







