define ["./../module"], (app)->
  app.factory "fbService", ($http) ->
    fbService =

      find:(name)->
        promise=$http.get(
          "./fb/friends/"+name
        )
        promise

      share: (message, link)->

        # $http returns a promise, which has a then function, which also returns a promise
        promise = $http.post("./fb/post/share", {
          message: message
          link: link
        }).then((response) ->

          # The then function here is an opportunity to modify the response
          if debug
            console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise


    fbService