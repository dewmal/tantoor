define [
  "./factory"
  "./event"
  "./eventType/eventType"
  "./event/userEvent"
  "./photo"
  "./user"
  "./fb/fbController"
]