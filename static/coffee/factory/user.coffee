define ["./module"], (app)->
  app.factory "userAuthenticationService", ($http) ->
    login: (username, password)->
      promise = $http.post('./request/user/login', {
        username: username
        password: password
      }).then((response)->
        response.data
      )
      promise
    register: (user)->
      promise = $http.post('./request/user/register', user).then((response)->
        response.data
      )
      promise

    validusername: (value)->
      promise = $http.get('./request/user/validusername/' + value).then((response)->
        response.data
      )
      promise

    validemail: (value)->
      promise = $http.get('./request/user/validemail/' + value).then((response)->
        response.data
      )
      promise

    getUserByid: (userid) ->

      # $http returns a promise, which has a then function, which also returns a promise
      promise = $http.get("./request/user/getby/id/" + userid).then((response) ->

        # The then function here is an opportunity to modify the response
        console.log response

        # The return value gets picked up by the then in the controller.
        response.data
      )

      # Return the promise to the controller
      promise

    curruntUser: ->

      # $http returns a promise, which has a then function, which also returns a promise
      promise = $http.get("./request/user/loggeduser").then((response) ->

        # The then function here is an opportunity to modify the response
        console.log response

        # The return value gets picked up by the then in the controller.
        response.data
      )

      # Return the promise to the controller
      promise