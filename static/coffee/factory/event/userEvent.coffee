define ["./../module"], (app)->
  app.factory "userEventListService", ($http) ->
    fstart = undefined
    flimit = undefined
    ftype = undefined
    _subcribers = []
    refresh = true
    userEventListService =
      setvalues: (start, limit, user) ->
        ftype = user or ftype
        flimit = limit or limit
        fstart = start or start
        return

      reset: (start, limit, user) ->
        refresh = true
        @set start, limit, user
        return

      set: (start, limit, user) ->
        ftype = user
        flimit = limit
        fstart = start
        ftype = ftype or "ALL"
        console.log flimit + " " + ftype + " " + fstart
        url = "./request/trip/byuser/"+user+"/" + fstart + "/" + flimit
        fstart += flimit

        # $http returns a promise, which has a then function, which also returns a promise
        promise = $http.get(url).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response
          angular.forEach _subcribers, (cb) ->
            cb response.data, refresh
            return

          refresh = false
          return
        )
        return


    # The return value gets picked up by the then in the controller.
    #return response.data;
      loadMore: ->
        @set fstart, flimit, ftype
        return

      subcribe: (func) ->
        _subcribers.push func
        return

    userEventListService