define ["./module"], (app)->
  app.factory "eventListService", ($http) ->
    fstart = undefined
    flimit = undefined
    ftype = undefined
    _subcribers = []
    refresh = true
    eventListService =
      setvalues: (start, limit, type) ->
        ftype = type or ftype
        flimit = limit or limit
        fstart = start or start
        return

      reset: (start, limit, type) ->
        refresh = true
        @set start, limit, type
        return

      set: (start, limit, type) ->
        ftype = type
        flimit = limit
        fstart = start
        ftype = ftype or "ALL"
        console.log flimit + " " + ftype + " " + fstart
        url = "./request/trip/all/" + fstart + "/" + flimit + "?type=" + ftype
        fstart += flimit

        # $http returns a promise, which has a then function, which also returns a promise
        promise = $http.get(url).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response
          angular.forEach _subcribers, (cb) ->
            cb response.data, refresh
            return

          refresh = false
          return
        )
        return


    # The return value gets picked up by the then in the controller.
    #return response.data;
      loadMore: ->
        @set fstart, flimit, ftype
        return

      subcribe: (func) ->
        _subcribers.push func
        return

    eventListService

  app.factory "eventPhotoListService" ,($http)->
    fstart = undefined
    flimit = undefined
    ftype = undefined
    _subcribers = []
    refresh = true
    eventPhotoListService =
      setvalues: (start, limit, event) ->
        ftype = event
        flimit = limit or limit
        fstart = start or start
        return

      reset: (start, limit, event) ->
        refresh = true
        @set start, limit, type
        return

      set: (start, limit, event) ->
        ftype = event
        flimit = limit
        fstart = start
        #ftype = ftype or "ALL"
        console.log flimit + " " + ftype + " " + fstart

        url = " ./request/photo/getphotosbyevent/" + ftype + "/"+fstart+"/" + flimit
        fstart += flimit

        # $http returns a promise, which has a then function, which also returns a promise
        promise = $http.get(url).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response
          angular.forEach _subcribers, (cb) ->
            cb response.data, refresh
            return

          refresh = false
          return
        )
        return


    # The return value gets picked up by the then in the controller.
    #return response.data;
      loadMore: ->
        @set fstart, flimit, ftype
        return

      subcribe: (func) ->
        _subcribers.push func
        return

    eventPhotoListService


  app.factory "eventService", ($http) ->
    eventService =
      getPhotos: (eventid, start, limit)->
        ###
        Load photos for given event
        ###
        promise = $http.get("./request/photo/getphotosbyevent/" + eventid + "/" + start + "/" + limit).then(
          (response) ->
            # The then function here is an opportunity to modify the response
            console.log response

            # The return value gets picked up by the then in the controller.
            response.data
        )
        promise

      publish: (event) ->
        promise = $http.post("./request/trip/publishevent", event).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise


      unpublish: (event) ->
        promise = $http.post("./request/trip/unpublish", event).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise

      photoCount: (eventid) ->
        promise = $http.get("./request/photo/getcountbytrip/" + eventid).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise

      getEvent: (eventid) ->
        promise = $http.get("./request/trip/get/" + eventid).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise

      getAllMembers: (tripid) ->
        promise = $http.get("./request/userhasevent/all/" + tripid).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise

      update: (event) ->


        # $http returns a promise, which has a then function, which also returns a promise
        promise = $http.put("./request/trip/update", event).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise

      delete: (event) ->


        # $http returns a promise, which has a then function, which also returns a promise
        promise = $http.post("./request/trip/delete/" + event.tripid).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise

      getLastUpdateUser: (tripid) ->

        # $http returns a promise, which has a then function, which also returns a promise
        promise = $http.get("./request/trip/updatehistory/0/1?tripid=" + tripid + "&lastupdate=12").then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise

      getEventByUserHasEvent: (userid) ->

        # $http returns a promise, which has a then function, which also returns a promise
        promise = $http.get("./request/trip/byuser/" + userid).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise

      updateCoverImagePosition: (eventid, postionY) ->
        option =
          coverphoto_top: postionY
          tripid: eventid
        # $http returns a promise, which has a then function, which also returns a promise
        promise = $http.post("./request/trip/updatecoverphototop", option).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise

    eventService
