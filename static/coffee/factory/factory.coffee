define ["./module"], (app)->

  app.factory "staticFile", ['$http', ($http)->
    get: (path)->
      promise = $http.get(path).then((response) ->
        return response.data
        # The return value gets picked up by the then in the controller.
        #response.data
      )

      promise

  ]
  