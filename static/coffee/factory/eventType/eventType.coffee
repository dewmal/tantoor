define ["./../module"], (app)->
  app.factory "eventTypeService", ($http) ->
    eventTypeService =
      all: ->

        # $http returns a promise, which has a then function, which also returns a promise
        promise = $http.get("./request/trip/eventtypes").then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise

      convertToObject: (id) ->

        # $http returns a promise, which has a then function, which also returns a promise
        promise = $http.get("./request/eventtype/get/" + id).then((response) ->

          # The then function here is an opportunity to modify the response
          console.log response

          # The return value gets picked up by the then in the controller.
          response.data
        )

        # Return the promise to the controller
        promise

    eventTypeService
