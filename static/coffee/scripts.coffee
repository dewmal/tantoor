bgSlideShow=->
  $.vegas("slideshow",
    backgrounds: [
      {
        src: "./static/img/loginbg.jpg"
        fade:4000
      }
      {
        src: "./static/img/loginbg2.jpg"
        fade:4000
      }
      {
        src: "./static/img/loginbg3.jpg"
        fade:4000
      }
    ]
  ) "overlay"

dialogue = (content, title) ->
  dialog = $("<div />")
  dialog.css('width', 'auto')
  dialog.qtip
    content:
      text: content
      title:
        text:title
        button:true


    position:
      my: "center"
      at: "center"
      target: $(window)

    adjust:
      method: 'shift flip'
    show:
      ready: true
      modal:
        on: true
        blur: false

    hide: false
    style:
      classes: "qtip-light qtip-bootstrap qtip-square dialogue"
      width: false
      height: false
    events:
      render: (event, api) ->
        $(".closedialog", api.elements.content).click (e) ->
          api.hide e
          return

        return

      hide: (event, api) ->
        api.destroy()
        return

  return



##nav-pills active controller

window.createGrowl = (title,message) ->
  persistent=window
  target = $(".qtip.jgrowl:visible:last")
  $("<div/>").qtip
    content:
      text: title
      title:
        text: message
        button: true

    position:
      target: [
        0
        0
      ]
      container: $("#qtip-growl-container")

    show:
      event: false
      ready: true
      effect: ->
        $(this).stop(0, 1).animate
          height: "toggle"
        , 400, "swing"
        return

      delay: 0
      persistent: persistent

    hide:
      event: false
      effect: (api) ->
        $(this).stop(0, 1).animate
          height: "toggle"
        , 400, "swing"
        return

    style:
      width: 250
      classes: "jgrowl"
      tip: false

    events:
      render: (event, api) ->
        unless api.options.show.persistent
          $(this).bind("mouseover mouseout", (e) ->
            lifespan = 2000
            clearTimeout api.timer
            if e.type isnt "mouseover"
              api.timer = setTimeout(->
                api.hide e
                return
              , lifespan)
            return
          ).triggerHandler "mouseout"
        return

  return