define ["./module"], (app)->

  ###
  ng-src="{{event.coverphoto|photo:300:0:75}}"/>
###
  app.filter "photo", ->
    (value, width, height, quality) ->
      #console.log(value);
      # //console.log(width);
      # console.log(height);
      re = new RegExp("^(http|https)://", "i")
      match = re.test(value)
      return value  if match
      if value
        quality = 70  unless quality
        url = "./image/timthumb.php?src=" + value
        url += "&w=" + width  if width and width isnt 0
        url += "&h=" + height  if height and height isnt 0
        url += "&w=1024"  if not height & not width
        url + "&zc=1&q=" + quality
      else
        "./static/img/user_1.png"

  ###
  Text limit for
  ###
  app.filter "limittext", ->
    (value, count) ->

      #console.log(input);
      if value
        if value.length > count
          value.substring(0, count - 3) + "..."
        else

          value
      else
        ""
