<?php
include_once 'components/header.php';
?>
<style type="text/css">
    html {
        background-image: url("./static/img/loginbg2.jpg");
        background-position: top;
        background-clip: content-box;
        background-size: 100% auto;
    }

    body {
        background-color: transparent;
    }
</style>
<div class="component" ng-controller="registerCtrl" id="registerPanel">
    <!--Bar-->
    <!--    <div id="login-wraper" style="background-color: rgba(0,0,0,0.2)">-->
    <div id="login-wraper" class="container"
         style="background-color: rgba(0,0,0,0.6);position: absolute;margin-right: auto;margin-left: auto;left: 0;right: 0;top:15%">

        <div class="col-md-12">
            <form class="form login-form col-md-6" action="./request/user/register" method="post">
                <legend>Sign up to <span class="blue label label-primary">tantoor</span></legend>

                <div class="body">

                    <p class="control-group">

                    <div class="right-inner-addon ">
                        <i class="fa fa-check-circle fa-2x green" ng-show="user.valid.firstname"
                           style="display: none"></i>
                        <i class="fa fa-times-circle fa-2x red" ng-show="user.error.firstname"
                           style="display: none"></i>
                        <input type="text"
                               name="firstname"
                               id="firstname"
                               ng-model="user.firstname"
                               ng-blur="validateFirstName()"
                               autofocus=""
                               class="form-control"
                               placeholder="Name"/>
                    </div>
                    </p>

                    <p class="control-group">

                    <div class="right-inner-addon ">

                        <i class="fa fa-check-circle fa-2x green" ng-show="user.valid.email" style="display: none"></i>
                        <i class="fa fa-times-circle fa-2x red" ng-show="user.error.email" style="display: none"></i>
                        <input type="text"
                               name="email"
                               id="email"
                               ng-model="user.email"
                               ng-blur="validateEmail()"
                               class="form-control"
                               placeholder="E-Mail"/>
                    </div>
                    </p>

                    <input type="hidden" name="username" ng-model="user.email">


                    <p class="control-group">

                    <div class="right-inner-addon ">

                        <i class="fa fa-check-circle fa-2x green" ng-show="user.valid.password"
                           style="display: none"></i>
                        <i class="fa fa-times-circle fa-2x red" ng-show="user.error.password" style="display: none"></i>
                        <input type="password"
                               name="password"
                               id="password"
                               ng-model="user.password"
                               ng-blur="validatePassword()"
                               class="form-control"
                               placeholder="Password"/>
                    </div>

                    </p>


                    <div class="control-group">
                        <div class="form-control" style="background-color: transparent">

                            <input name="gender" type="radio" id="flat-radio-1" ng-model="user.gender" value="male">
                            <label for="flat-radio-1">Male</label>

                            <input name="gender" type="radio" id="flat-radio-1" ng-model="user.gender" value="female">
                            <label for="flat-radio-1">Female</label>


                        </div>
                    </div>


                </div>


                <div class="footer">
<a href="#" style="color: #f5f5f5">Agree with term &amp; Conditons
</a>
                    <button disabled ng-disabled="invalid" class="btn btn-success btn-square pull-right"
                            ng-click="registerUser()">Register
                    </button>
                </div>

            </form>

            <div class="col-md-6 sidepanel">

                <h2 style="color: #f5f5f5">
                    Join us today !
                </h2>

                <h3 style="color: #f5f5f5">
                    Connect with your favourite Social Network
                </h3>
                <a class="btn btn-block btn-social btn-facebook">
                    <i class="fa fa-facebook"></i> Sign in with Facebook
                </a>

                <a class="btn btn-block btn-social btn-twitter">
                    <i class="fa fa-twitter"></i> Sign in with Twitter
                </a>
            </div>

        </div>
    </div>


    <div class="container">
        <footer class="white navbar-fixed-bottom"
                style="padding: 10px;height: 70px;background-color: rgba(255,255,255,0.5);color: #000000">
            Do you have an account ? <a href="./userauth" class="btn btn-danger btn-lg btn-square">Sign in</a>
        </footer>
    </div>
</div>

<?php
include_once 'components/footer.php';
?>


