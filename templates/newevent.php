<?php
include_once 'components/header.php';
?>


<div class="component">


    <!--Bar-->
    <!--    <div id="login-wraper" style="background-color: rgba(0,0,0,0.2)">-->
    <div class="col-xs-12 panel"
         ng-controller="newEventCtrl"
         style="background-color: rgba(255,255,255,1);position: absolute;margin-right: auto;margin-left: auto;left: 0;right: 0;top:25%">

        <div class="col-md-12">
            <h2>Start publishing your event in <label class="label label-primary">tantoor</label></h2>
            <hr/>
            <form action="./request/trip/new" method="post">

                <div class="form-group">
                    <input ng-model="name" name="name" id="name" class="form-control form-control-square"
                           placeholder="Event Name "
                           style="line-height: 24px;font-size: 24px;height: auto">
                </div>
                <div class="form-group">
                    <select   id="type" name="type" class="form-control form-control-square"
                            style="line-height: 24px;font-size: 24px;height: auto">


                        <?php

                        foreach (json_decode($eventTypes) as $eventType) {
                            echo '<option value="' . $eventType->id . '">' . $eventType->name . '</option>';
                        }


                        // foreach ($eventTypes as $eventType) {

                        // }


                        ?>


                    </select>
                </div>
                <div class="form-group">

                    <button type="submit" disabled ng-disabled="btnDisabled" class="btn btn-lg btn-primary btn-square pull-right">
                        Start uploading photos

                        <span class="fa fa-caret-right "></span>
                    </button>
                </div>
            </form>

        </div>
    </div>


</div>



<?php
include_once 'components/footer_simple.php';
?>
