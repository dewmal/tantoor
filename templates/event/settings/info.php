<div class="panel col-xs-12">

    <div class="col-xs-12">
        <form class="bf-example form-horizontal form-square" ng-controller="eventUpdate"
              style="background-color: #fcfcfc;padding: 10px;min-width: 500px;">
            <legend>Update event details</legend>
            <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 control-label">Name</label>

                <div class="col-lg-10">
                    <input type="text" ng-model="event.name" class="form-control" id="inputEmail1" placeholder="Event Name">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 control-label">Description</label>

                <div class="col-lg-10">
                    <textarea ng-model="event.description" rows="6" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 control-label">Category</label>

                <div class="col-lg-10">
                    <select  ui-select2 class="form-control" id="event_type_select" ng-model="event_type">
                        <option ng-repeat="eventtype in eventTypes" value="{{ eventtype.id }}" ng-selected="event_type==event.event_type">{{ eventtype.name }}</option>
                        <option value="Other">Other</option>
                    </select>
                    <input type="email" class="form-control" id="otherEvent"

                           style="display: none;padding-top: 5px;"
                           placeholder="Other event name">

                </div>
            </div>


            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <button
                              server-click="update()"
                              server-update-message="Updating..."
                              type="submit" class="btn btn-primary">
                        <!--<span class="fa fa-refresh"></span>-->
                        Save Changes
                    </button>
                    <button ng-click="load()" type="submit" class="btn btn-default">
                        <!--<span class="fa fa-"></span>-->
                        Cancel
                    </button>
                </div>
            </div>
        </form>

    </div>

</div>