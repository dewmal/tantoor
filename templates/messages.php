<?php
include_once 'components/header.php';
?>
<style type="text/css">
    html {
        background-image: url("./static/img/loginbg.jpg");
        background-position: top;
        background-clip: content-box;
        background-size: 100% auto;
    }

    body {
        background-color: transparent;
    }
</style>
<div class="component">

    <!--Bar-->
    <!--    <div id="login-wraper" style="background-color: rgba(0,0,0,0.2)">-->
    <div id="login-wraper" class="container"
         style="background-color: rgba(0,0,0,0.6);position: absolute;margin-right: auto;margin-left: auto;left: 0;right: 0;top:25%">

        <div class="col-md-12">

            <h1 style="color: #f5f5f5">
                <?php echo $title ?>
            </h1>

            <h2 style="color: #f5f5f5">
                <p class="label-<?php echo $type ?>">
                    <?php echo $message ?>
                </p>
            </h2>

        </div>
    </div>

    <div class="container">
        <footer class="white navbar-fixed-bottom"
                style="padding: 10px;height: 70px;background-color: rgba(255,255,255,0.5);color: #000000">
            Don't have an account yet? <a href="./userauthnew" class="btn btn-danger btn-lg btn-square">Sign up</a>
        </footer>
    </div>

</div>

<?php
include_once 'components/footer.php';
?>


