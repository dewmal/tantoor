<?php

include_once 'components/header.php';
?>

    <div class="component" ng-controller="ownerProfileCtrl" id="userViewPage"
        >
        <!--Bar-->
        <div class="col-xs-2" style="">


        <user-component-lg></user-component-lg>


        </div>
        <div class="col-xs-7" style="border-left: 1px #f5f5f5 dashed"
             when-scrolled="loadMore()">


            <owner-component ng-repeat="event in events" event="event" ></owner-component>


            <div ng-hide="events" class="col-xs-12 panel">
                <h4>
                    You don't have any event
                </h4>
                <a href="/newevent">Post your event hear</a>
            </div>


        </div>
        <div class="col-xs-3">
            <div class="col-xs-12 panel" id="affix" style="padding: 0px;">
                <a class="btn btn-danger btn-lg btn-square col-xs-12" href="./newevent">
                    <span class="fa fa-plus"></span>
                    Add Event</a>
            </div>


        </div>

    </div>


<?php
include_once 'components/footer.php';
?>