<?php
include_once 'components/header.php';
?>

    <div class="component">
        <!--Bar-->
        <div class="col-xs-2" style="">
            <user-component-lg></user-component-lg>

        </div>
        <div class="col-xs-7" ng-controller="EventViewCtrl" style="border-left: 1px #f5f5f5 dashed" when-scrolled="loadMore()">
            <event-component-linear ng-repeat="event in events" event="event" ></event-component-linear>
        </div>
        <div class="col-xs-3">
            <div class="col-xs-12 panel" id="affix" style="padding: 0px;">
                <a class="btn btn-danger btn-lg btn-square col-xs-12" href="./newevent">
                    <span class="fa fa-plus"></span>
                    Add Event</a>
            </div>
            <div class="col-xs-12 panel">


                <?php foreach ($randomPhotos as $photo) {
                    ?>
                    <a href="./<?php echo($photo['trip']) ?>">
                        <div
                            class="col-xs-4"
                            style="float: left;height: 75px;
                                background-image: url('./image/timthumb.php?src=<?php echo($photo['url']); ?>&w=150&zc=1&q=100');background-size: cover">

                        </div>
                    </a>

                <?php
                }
                ?>


            </div>

            <div class="col-xs-12 panel" style="padding-right: 0px">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- tantoor right bar -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:300px;height:250px"
                     data-ad-client="ca-pub-1813863267368130"
                     data-ad-slot="2837626827"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>

        </div>
    </div>
<?php
include_once 'components/footer-home.php';
?>


