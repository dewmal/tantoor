<?php
include_once 'components/header.php';
?>


<div class="component">


    <!--Bar-->
    <!--    <div id="login-wraper" style="background-color: rgba(0,0,0,0.2)">-->
    <div class="container panel"
         ng-controller="updateUserDetails"
         style="background-color: rgba(255,255,255,1);position: absolute;margin-right: auto;margin-left: auto;left: 0;right: 0;top:25%">

        <div class="col-md-12">
            <h2>Change your profile details </h2>
            <hr/>
            <form action="./request/user/update" method="post" enctype="multipart/form-data">

                <div class="form-group">
                    <input ng-model="name" name="firstname" id="firstname" class="form-control form-control-square"
                           placeholder="First Name "
                           value="<?php echo $user['firstname'] ?>"
                           style="line-height: 24px;font-size: 24px;height: auto">
                </div>

                <div class="form-group">
                    <input ng-model="name" name="name" id="name" class="form-control form-control-square"
                           placeholder="Email"
                           disabled
                           value="<?php echo $user['email'] ?>"
                           style="line-height: 24px;font-size: 24px;height: auto">
                </div>

                <div class="form-group">
                    <input ng-model="name" name="name" id="name" class="form-control form-control-square"
                           placeholder="Username"
                           disabled
                           value="<?php echo $user['username'] ?>"
                           style="line-height: 24px;font-size: 24px;height: auto">
                </div>


                <div class="form-group">
                    <a onclick="$('#profile_details_propic').show(200);$(this).hide(200)" class="link">Change profile
                        picture</a>
                    <input ng-model="propic" name="propic" id="profile_details_propic"
                           class="form-control form-control-square"
                           placeholder="Profile pic"
                           type="file"
                           style="line-height: 24px;font-size: 24px;height: auto;display: none">
                </div>

                <a onclick="$('#profile_details_newpassword').show(200);$(this).hide(200)" class="link">Change
                    password</a>

                <?php if ($invalidpassword == "true") {
                    ?>
                    <div class="alert alert-danger alert-square">
                        <h4 style="color: #f5f5f5">Invalid Current password</h4>
                    </div>

                <?php
                }?>


                <div class="form-group" id="profile_details_newpassword" style="display: none">

                    <input ng-model="name" type="password"  name="password" name="idpassword"
                           class="form-control form-control-square"
                           placeholder="New password"
                           style="line-height: 24px;font-size: 24px;height: auto;">

                    <p></p>
                    <input ng-model="name" type="password" name="oldpassword" id="oldpassword" class="form-control form-control-square"
                           placeholder="Old Password"
                           style="line-height: 24px;font-size: 24px;height: auto">
                </div>


                <div class="form-group">

                    <button type="submit" ng-disabled="btnDisabled"
                            class="btn btn-lg btn-primary btn-square pull-right">
                        Update profile

                        <span class="fa fa-caret-right "></span>
                    </button>
                </div>
            </form>

        </div>
    </div>


</div>



<?php
include_once 'components/footer.php';
?>
