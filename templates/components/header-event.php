<!DOCTYPE html>
<html>
<head>

    <?php
    //print_r($event);
    if (isset($event) && property_exists($event, 'name')) {
        ?>


        <?php
        $eventName = $event->name;
        echo "<title>$eventName | Tantoor.com</title>";

    } else {
        ?>
        <title>Share Events | Publish Events | Update Event | Tantoor.com</title>
    <?php } ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='width=1190'>
    <meta name="Keywords"
          content="event, photo, photography, disposable, camera, share, sharing, day, big, community, friends, social, social network,team, image"/>
    <meta name="Description" content="The easiest way to share your events among friends"/>

    <meta property="og:image" content="<?php
    if (array_key_exists('ogimage', get_defined_vars()) ) {
        echo "http://tantoor.com/image/timthumb.php?src=".$ogimage."&h=200&zc=1&q=100";
    } else {
        echo "http://tantoor.com/static/img/loginbg2.jpg";
    }

    ?>"/>

    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <!-- Bootstrap -->
    <!--    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300italic' rel='stylesheet' type='text/css'>-->
    <link href="./static/css/style.css" rel="stylesheet" media="screen">
    <link href="./static/css/nonresponsive.css" rel="stylesheet" media="screen">


    <link rel="stylesheet" href="./static/vendor/lightbox/css/lightbox.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="./static/vendor/bootflat/js/html5shiv.js"></script>
    <script src="./static/vendor/bootflat/js/respond.min.js"></script>

    <![endif]-->


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-48625038-1', 'tantoor.com');
        ga('send', 'pageview');

    </script>


    <script data-main="static/scripts/main" src="./static/vendor/requirejs/require.js"></script>

</head>
<body style="padding-bottom: 0px;">
<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="col-xs-12">
        <div class="navbar-header">
            <h5 style="margin: 0px;padding: 0px;">
                <a class="navbar-brand" href="/">

                <strong>
                        tantoor
                    </strong>
                    <sub>

                        <small>
                            Beta
                        </small>


                    </sub>

                </a>
            </h5>
        </div>

        <div class="navbar-form navbar-left  col-xs-3">
<!--            <div class="form-square search-only ">-->
<!--                <input type="text" class="form-control search-query">-->
<!--            </div>-->
        </div>

        <div class="col-xs-3  navbar-right">
            <ul class="nav navbar-nav pull-right">

                <?php if (array_key_exists("user", $_SESSION) && is_array($_SESSION['user'])) {

                    ?>
                    <li>
                        <a href="/profile" style="padding-top:8px;padding-bottom: 8px;">

                            <?php

                            if (array_key_exists("user", $_SESSION)) {
                                $userDetails = $_SESSION['user'];
                            } else {
                                $userDetails = array();
                            }
                            if (array_key_exists("propic", $userDetails)) {

                                ?>
                                <img src="<?php echo $userDetails['propic'] ?>"
                                     style="max-width: 30px;max-height: 30px;">

                            <?php

                            } else {
                                ?>
                                <img src="./static/img/user_1.png"
                                     style="max-width: 30px;max-height: 30px;">

                            <?php
                            }?>


                            Me
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="border-right: 1px solid #3c4680">
                            <span class="fa fa-cogs"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/accountsettings">Account Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="/request/user/logout">Logout</a></li>
                        </ul>
                    </li>

                <?php
                } else {

                    ?>
                    <li>
                        <a href="./userauth">Sign in</a>
                    </li>
                <?php
                } ?>


            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>
