<?php
include_once 'components/header.php';
?>
<style type="text/css">
    html {
        background-image: url("./static/img/loginbg2.jpg");
        background-position: top;
        background-clip: content-box;
        background-size: 100% auto;
    }

    body {
        background-color: transparent;
    }
</style>
<div class="component" ng-controller="loginCtrl" id="loginPanel" >


    <!--Bar-->
    <!--    <div id="login-wraper" style="background-color: rgba(0,0,0,0.2)">-->
    <div id="login-wraper" class="container"
         style="background-color: rgba(0,0,0,0.6);position: absolute;margin-right: auto;margin-left: auto;left: 0;right: 0;top:25%">

        <div class="col-md-12">
            <form class="form login-form col-md-6" action="./request/user/login" method="post">
                <legend>Sign in to <span class="blue label label-primary">tantoor</span></legend>


                <input type="hidden" name="redirectPageLocation" value="<?php echo $redirectPage; ?>"
                       id="redirectPageLocation">

                <div class="body">
                    <p class="control-group">
                        <input type="text" class="form-control" name="username" id="username" ng-model="user.username"
                               placeholder="User name is your email...">
                    </p>

                    <p class="control-group">
                        <input type="password" id="password" name="password" placeholder="Password..."
                               ng-model="user.password" class="form-control">
                    </p>



                    <?php
                    if ($invalid) {
                        ?>
                        <label class="label label-danger" style="color: #FFFFFF;width: 100%" >
                        <small>
                            Invalid login details! Please <t></t>ry again
                        </small>
                    </label>
                    <?php } ?>


                </div>


                <div class="footer">
                    <label class="checkbox inline">
                        <input type="checkbox" id="inlineCheckbox1" value="option1"> Remember me
                    </label>

                    <button type="submit" class="btn btn-success btn-square" ng-click="loginAction()">Login</button>
                </div>
                <a href="/resetpassword">
                    Forgot password
                </a>
            </form>

            <div class="col-md-6 sidepanel">

                <h2 style="color: #f5f5f5">
                    Join us today !
                </h2>

                <h3 style="color: #f5f5f5">
                    Connect with your favourite Social Network
                </h3>
                <a class="btn btn-block btn-social btn-facebook" href="/fb/login">
                    <i class="fa fa-facebook"></i> Sign in with Facebook
                </a>

                <a class="btn btn-block btn-social btn-twitter">
                    <i class="fa fa-twitter"></i> Sign in with Twitter
                </a>


            </div>

        </div>
    </div>

    <div class="container">
        <footer class="white navbar-fixed-bottom"
                style="padding: 10px;height: 70px;background-color: rgba(255,255,255,0.5);color: #000000">
            Don't have an account yet? <a href="./userauthnew" class="btn btn-danger btn-lg btn-square">Sign up</a>
        </footer>
    </div>

</div>

<?php
include_once 'components/footer_simple.php';
?>


