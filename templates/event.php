<?php

$evntCtrl = new \App\TripCtrl();
$ogimage = $evntCtrl->getCoverPhoto($pageid);

include_once 'components/header-event.php';
?>

    <div class="component" id="eventViewPage" ng-controller="EventCtrl" eventid="<?php echo($pageid) ?>" style="padding-top: 30px;display: none""


        >

        <div id="eventCoverPhoto" style="
    width: 100%;
   height: 350px;overflow: hidden;position: relative;
   background-image: url('{{ event.coverphoto|photo:1024:null:100 }}');
   background-position: 0px {{ event.coverphoto_top }}px;
   background-repeat: no-repeat;
   background-size: cover;
   "
            >

            <div
                id="messageBoard"
                style="position: absolute;display: none; width: 100%;top:175px;background-color: rgba(255,255,255,0.5);text-align: center">
                <h4 >

                    <span  class="label label-primary">
                        <span class="fa fa-align-justify"></span>
                        Drag &amp; Reposition
                    </span>

                </h4>
                <button style="display: none" class="btn btn-primary">
                    <span class="fa fa-check"></span>
                    Done</button>
            </div>

            <!--            <img id="coverPhoto"-->
            <!--                 src="{{ event.coverphoto|photo:1024:null:100 }}"-->
<!--                 style="position: absolute; width: 100%;-->
<!--                 height: 100%;-->
<!--                                   cursor: all-scroll;-->
<!--                                   top:200 "/>-->

        </div>
        <div class="col-xs-10">
            <div style="width: 100%;height: 50px;background-color: rgba(0,0,0,0.84);
        padding: 10px;
        position: relative;margin-top:-50px;">
<span class="pull-left" style="padding: 0px;">

                    <h4 style="padding: 0px;margin: 0px;color: #f5f5f5">
                        <p class="">
                        <i class="fa fa-quote-left text-muted  fa-fw"
                               style="font-size: .5em;color: #9f9f9f;vertical-align: super"></i>
                            {{ event.name }}</p>
                    </h4>
</span>

            <a href="#" class="pull-right" style="color: #f5f5f5;font-size: 1.4em;">
                <small>
                    # {{ event.event_type.name }}
                </small>
            </a>
            </div>
        </div>
        <div class="col-xs-10" style="">


            <div class="panel" style="margin-bottom: 0px;" ng-show="isowner">
                <div class="pull-right">
                    <button class="btn btn-normal btn-square" ng-click="updateEventDetails()">
                        <span class="fa fa-wrench"></span>
                        Edit
                    </button>
                    <button class="btn btn-primary btn-square"  ng-hide="event.published" ng-click="publish()">
                        <span class="fa fa-globe"></span>
                        Publish
                    </button>

                    <button class="btn btn-primary btn-square"  ng-show="event.published" ng-click="unpublish()">
                        <span class="fa fa-ban"></span>
                        Unpublish
                    </button>


                    <div class="btn-group btn-group-square">
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <span class="fa fa-camera-retro"></span>

                            Cover

                            photo
                        </button>
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <input ng-file-select="fileUploadOptionsCoverPhoto" type="file" class="hidden"
                               id="uploadCoverImage">
                        <ul class="dropdown-menu" role="menu">
                            <li><a onclick="$('#uploadCoverImage').click();">
                                    <span class="fa fa-upload"></span>
                                    Upload</a></li>
                            <li><a
                                    ng-click="changeBackGroundPositon()"
                                    href="#">
                                    <span class="fa fa-arrows"></span>
                                    Reposition</a></li>
                        </ul>
                    </div>


                </div>


                <div class="clearfix"></div>

            </div>


            <div  id="links" when-scrolled="loadPhotos()" style="padding: 0px">
                <div class="col-xs-12" id="control-panel"
                     style="height: auto;padding: 0px;position: relative;display: none"></div>

                <div class="col-xs-4" style="margin-top: 10px;">


                    <div class="col-xs-12 item panel">

                        <blockquote style="text-align: justify">
                            <small>
                                {{ event.description }}
                            </small>
                        </blockquote>
                    </div>

                    <event-photo class="col-xs-12 panel item"
                                 style="" ng-repeat="photo in event.photos"
                                 ng-show="photo.url"
                                 photo="photo"
                                 onremove="trigerOnDelete($index)"
                                 data-price="20"></event-photo>

                </div>
                <div class="col-xs-4" style="margin-top: 10px;">


                    <div class="col-xs-12  item  panel stamped-second functionCard" id="uploadImageView"

                         invite-friends

                         style="text-align: center;background-color: transparent;">

                        <h3 class="title">
                            <span class="fa fa-user"></span>
                            Invite Friends</h3>

                        <h1>
                            <span class="fa fa-facebook functionCardicon"></span>
                            <span class="fa fa-envelope functionCardicon"></span>
                            <span class="fa fa-twitter functionCardicon"></span>
                        </h1>
                    </div>


                    <event-photo class="col-xs-12 panel item"
                                 style="" ng-repeat="photo in event.photos1"
                                 ng-show="photo.url"
                                 photo="photo"
                                 onremove="trigerOnDelete($index)"
                                 data-price="20"></event-photo>

                </div>

                <div class="col-xs-4" style="margin-top: 10px;">
                    <div class=" col-xs-12 item panel stamp-second" id="bengSc" style="">
                        <button class="btn btn-primary btn-square">
                            <span class="fa fa-thumbs-up"></span>
                            Like
                        </button>
                        <button share-event event="event" class="btn btn-primary btn-square">
                            <span class="fa fa-comments"></span>
                            Share
                            </button>
                        </div>


                    <div class="col-xs-12   item   panel stamped-second functionCard" id="uploadImage"
                         ng-show="isowner"
                         style="text-align: center;background-color: transparent;"
                         ng-click="showImageUploadView()">

                        <h3 class="title">
                            <span class="fa fa-plus-circle"></span>
                            Upload Photos</h3>

                        <h1>
                            <span class="fa fa-camera-retro fa-5x functionCardicon"></span>
                        </h1>

                        <button class="btn btn-primary btn-square btn-lg center-block">Select Photos</button>


                        <!--<input ng-file-select type="file" multiple style="display: none" id="photouploader"/>-->

                    </div>

                    <event-photo class="col-xs-12 panel item"
                                 style="" ng-repeat="photo in event.photos2"
                                 ng-show="photo.url"
                                 photo="photo"
                                 onremove="trigerOnDelete($index)"
                                 data-price="20"></event-photo>


                </div>
            </div>


        </div>

        <div class="col-xs-2" style="padding-top: 10px;">

            <div class="col-xs-12 panel">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- tantoor right bar -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:160px;height:600px"
                     data-ad-client="ca-pub-1813863267368130"
                     data-ad-slot="2837626827"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
    </div>



<?php
include_once 'components/footer-event.php';
?>

