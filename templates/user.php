<?php

include_once 'components/header.php';
?>

    <div class="component" id="userViewPage" ng-controller="viewProfileCtrl" userid="<?php echo($pageid) ?>"
         style="display: none">
        <!--Bar-->
        <div class="col-md-2" style="">
            <user-component-lg></user-component-lg>



        </div>
        <div class="col-md-7" style="border-left: 1px #f5f5f5 dashed"
             when-scrolled="loadMore()">

            <div class="panel col-xs-12">
                <a event-add class="btn btn-primary  btn-square">
                    <span class="fa fa-heart-o"></span>
                    Follow</a>
            </div>


            <event-component-linear ng-repeat="event in events" event="event"></event-component-linear>
        </div>
        <div class="col-md-3">
            <div class="col-md-12 panel" id="affix" style="padding: 0px;">

            </div>


        </div>

    </div>


<?php
include_once 'components/footer.php';
?>