<?php
include_once 'components/header.php';
?>
<style type="text/css">
    html {
        background-image: url("./static/img/loginbg.jpg");
        background-position: top;
        background-clip: content-box;
        background-size: 100% auto;
    }

    body {
        background-color: transparent;
    }
</style>
<div class="component">
    <!--Bar-->
    <!--    <div id="login-wraper" style="background-color: rgba(0,0,0,0.2)">-->
    <div id="login-wraper" class="container"
         style="background-color: rgba(0,0,0,0.6);position: absolute;margin-right: auto;margin-left: auto;left: 0;right: 0;top:25%">

        <div class="col-xs-12">
            <form class="form login-form col-xs-6" action="./thanksconfrimmail" method="get">
                <legend>Confirm you <span class="blue label label-info">E-Mail</span></legend>

                <div class="body">
                    <p class="control-group">
                        <input type="text" class="form-control" name="username" placeholder="Username">
                    </p>

                    <p class="control-group">
                        <input type="text" class="form-control" name="confirm" placeholder="Confirmcode">
                    </p>
                </div>


                <div class="footer">
                    <button type="submit" class="btn btn-success btn-square">Confirm email
                    </button>
                </div>

            </form>

            <div class="col-xs-6 sidepanel">

                <h2 style="color: #f5f5f5">
                    Welcome tantoor
                </h2>

                <h3 style="color: #f5f5f5">
                    Connect with your favourite Social Network
                </h3>
                <a class="btn btn-block btn-social btn-facebook">
                    <i class="fa fa-facebook"></i> Sign in with Facebook
                </a>

                <a class="btn btn-block btn-social btn-twitter">
                    <i class="fa fa-twitter"></i> Sign in with Twitter
                </a>
            </div>

        </div>
    </div>

    <div class="container">
        <footer class="white navbar-fixed-bottom"
                style="padding: 10px;height: 70px;background-color: rgba(255,255,255,0.5);color: #000000">
            Don't have an account yet? <a href="./userauthnew" class="btn btn-danger btn-lg btn-square">Sign up</a>
        </footer>
    </div>

</div>

<?php
include_once 'components/footer_simple.php';
?>


