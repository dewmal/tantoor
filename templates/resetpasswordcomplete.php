<?php
include_once 'components/header.php';
?>
<style type="text/css">
    html {
        background-image: url("./static/img/loginbg3.jpg");
        background-position: top;
        background-clip: content-box;
        background-size: 100% auto;
    }

    body {
        background-color: transparent;
    }
</style>
<div class="component">
    <!--    <div id="login-wraper" style="background-color: rgba(0,0,0,0.2)">-->
    <div id="login-wraper" class="container"
         style="background-color: rgba(0,0,0,0.6);position: absolute;margin-right: auto;margin-left: auto;left: 0;right: 0;top:25%">

        <div class="col-xs-12">
            <form class="form login-form col-xs-6">
                <legend>Welcome to <span class="blue label label-primary">tantoor</span></legend>

                <h3 style="color: #f5f5f5">Password reset success <span class="fa fa-check"></span></h3>
                <h4 style="color: #fff">Please check your email for new password</h4>
                <a href="/newevent" class="btn btn-danger btn-lg btn-square">Sign in hear <span
                        class="fa fa-caret-right"></span></a>

            </form>

            <div class="col-xs-6 sidepanel">

                <h2 style="color: #f5f5f5">
                    Invite your <span class="blue"
                                      style="text-decoration: underline;text-decoration-color: #346fff;text-decoration-line: underline">Friends</span></legend>
                </h2>


                <a class="btn btn-block btn-social btn-facebook">
                    <i class="fa fa-facebook"></i> Using Facebook
                </a>

                <a class="btn btn-block btn-social btn-twitter">
                    <i class="fa fa-twitter"></i> Using Twitter
                </a>
            </div>

        </div>
    </div>

    <div class="container">
        <footer class="white navbar-fixed-bottom"
                style="padding: 10px;height: 70px;background-color: rgba(255,255,255,0.5);color: #000000">
            Don't have an account yet? <a href="./userauthnew" class="btn btn-danger btn-lg btn-square">Sign up</a>
        </footer>
    </div>

</div>

<?php
include_once 'components/footer_simple.php';
?>


