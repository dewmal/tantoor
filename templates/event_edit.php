<?php

$evntCtrl = new \App\TripCtrl();
$event = $evntCtrl->getEvent($pageid);
$event = json_decode($event);

include_once 'components/header-event.php';
?>

<div class="component" id="eventSettingsViewPage" ng-controller="EventSettingsCtrl" eventid="<?php echo($pageid) ?>"
     style="padding-top: 35px;""     >


<div class="col-xs-10" style="padding-right: 0px;">


    <div class="col-xs-12 panel">

        <div class="pull-left">
            <h4>
                <a href="/<?php echo($pageid); ?>">
                    <?php echo($event->name); ?>
                </a>
            </h4>


        </div>
        <h3 class="pull-right">
            Event manager
        </h3>

        <div class="clearfix"></div>

    </div>

    <div class="col-xs-3 " style="padding-left:0;padding-right: 10px;">
        <div class="panel list-group">
            <a href="?settings=photos" class="list-group-item ">
                <span class="fa fa-camera-retro"></span>
                Photos </a>

            <a href="?settings=info" class="list-group-item">
                <span class="fa fa-info-circle"></span>
                Event info</a>
            <a href="?settings=members" class="list-group-item ">
                <span class="fa fa-user"></span>
                Members</a>
            <a href="?settings=privacy" class="list-group-item ">
                <span class="fa fa-lock"></span>
                Privacy</a>

        </div>
    </div>

    <div class="col-xs-9 pull-right" style="padding: 0px;">

        <?php include_once "event/settings/$settings.php"; ?>


    </div>


</div>

<div class="col-xs-2" style="padding-top: 10px;">


</div>
</div>



<?php
include_once 'components/footer.php';
?>

