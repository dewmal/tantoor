<?php
//echo"done1";
session_cache_limiter(false);
session_start();
function ob_html_compress($buf){
    return str_replace(array("\n","\r","\t"),'',$buf);
}

ob_start("ob_html_compress");

//header('Content-Type: application/json');

    include_once './lib/Slim/Slim.php';
    include_once './lib/activerecode/ActiveRecord.php';
    include_once './lib/fbbase/facebook.php';
    include_once './utill.php';
    include_once './controller/image/image.php';
    include_once "./lib/sendgrid/sendgrid-php.php";
   // echo"done2";

    \Slim\Slim::registerAutoloader();
   // include './lib/Slim/views/Twig.php';
  //  include './lib/Slim/views/TwigExtension.php';

    $app = new \Slim\Slim(
            array('debug' => true,
        'log.enabled' => true,
        'log.level' => \Slim\Log::DEBUG,
        "log.writer" => new \Slim\TimestampLogFileWriter(array('path' => './logs',
            'mode' => 'development')))
    );

   // echo"done2";
//$app->log->debug("This is a test from the logger...");
//$app->log->setEnabled(true);
//$app->add(new App\LocationHandler());
//Enable logging
//$app->log->setEnabled(true);
//Reqsuest Controllers
    include_once './controller/LocationRequestController.php';

    include_once './controller/PhotoCtrl.php';

    include_once './controller/TripCtrl.php';
    include_once './controller/UserReqCtrl.php';
    include_once './controller/FbCtrl.php';

    include_once './controller/UserHasEventCtrl.php';
    include_once './controller/EventTypeReqCtrl.php';
    include_once './controller/LikeReqCtrl.php';
    include_once './controller/CommentReqCont.php';

    //
    include_once './controller/EmailCtrl.php';

//DB modals
    include_once './orm/modals/modals.php';
  //  echo"done4";
    include_once './orm/cont/AbsCtrl.php';
  //  echo"done5";
    //include_once './orm/cont/Location.php';
  //  echo"done6";
    include_once './orm/cont/Photo.php';
  //  echo"done7";
    include_once './orm/cont/Trip.php';

    include_once './orm/cont/User.php';
    include_once './orm/cont/UserHasEvents.php';
    include_once './orm/cont/NewUser.php';
    include_once './orm/cont/EventType.php';
    include_once './orm/cont/UpdateHistory.php';
    include_once './orm/cont/LikeCtrl.php';
    include_once './orm/cont/CommentCtrl.php';


    new App\EmailCtrl($app);
    new App\LocationHandler($app);
    new App\PhotoReqCtrl($app);
    new App\TripReqCtrl($app);
    new App\EventTypeReqCtrl($app);
$userReqCtrl = new App\UserReqCtrl($app);

new App\FbCtrl($app);
new App\UserHasEventReqCtrl($app);

    new App\LikeReqCtrl($app);
    new App\CommentReqCtrl($app);


// Prepare view
   // $app->view(new \Slim\Views\Twig());
    $app->view->parserOptions = array(

        'charset' => 'utf-8',
        'cache' => realpath('./cache'),
        'auto_reload' => true,
        'strict_variables' => false,
        'autoescape' => true
    );
//    $app->view->parserExtensions = array(new \Slim\Views\TwigExtension());

//    $app->get('/image/:tripid/:photoname', function ($tripid = NULL, $photoname = NULL) use ($app) {
//
//
//        $imageurl = ".." . DIRECTORY_SEPARATOR . "storage" . DIRECTORY_SEPARATOR . "trip" . DIRECTORY_SEPARATOR . $tripid . DIRECTORY_SEPARATOR . $photoname;
//        echo getcwd();
//        $image = file_get_contents($imageurl);
//        $app->response()->header('Content-Type', 'content-type: image/jpg');
//        echo $image;
//    });


   // echo"done4";

    include_once 'viewloader.php';

    $app->run();

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
ob_end_flush();
?>

