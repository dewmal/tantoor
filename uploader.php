<?php
include_once './utill.php';
include_once './controller/image/image.php';
/**
 * @author Resalat Haque
 * @link http://www.w3bees.com
 */
header('Content-type: application/json');



if (!empty($_FILES)) {
    try {
        
        $image=new Image();
        
        $tempPath = $_FILES['file']['tmp_name'];
        $imageName= $_FILES['file']['name'];
        $imageType= $_FILES['file']['type'];
        $json = json_encode($image->upload($tempPath,$imageName));
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }


    echo $json;
} else {
    echo 'No files';
}

//echo 'asdasdasd';

// echo out json encoded status
?>