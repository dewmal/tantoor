<?php

namespace App;

use Slim;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class UserReqCtrl
{

    private $url = "/request/user";

    function __construct(Slim\Slim $app)
    {

        $user = new UserCtrl();
        $newUser = new NewUserCtrl();


        $app->get($this->url . "/loggeduser", function () use ($app, $user) {
            $app->contentType('application/json');
            if (key_exists("userid", $_SESSION)) {
                $value = $_SESSION['userid'];
                echo json_encode($user->byUserID($value));
            } else {
                echo json_encode(json_decode("{}"));
            }
        });


        $app->get($this->url . "/userexprience/:userid", function ($userid) use ($app, $newUser) {
            $app->contentType('application/json');
            echo json_encode($newUser->getFromUserid($userid));
        });

        $app->post($this->url . '/find/', function () use ($app, $user) {
            $app->contentType('application/json');
            //  print_r($app->request()->params());
            $query = $app->request()->params('query');
            $limit = $app->request()->params('limit');
            //echo $limit;
            // echo $query;
            // echo $_SESSION['userid'];
            echo json_encode($user->findByAnything($query, $limit, $_SESSION['userid']));
        });


        /**
         * Randomly get users
         */
        $app->get($this->url . '/trips/:userid/', function ($userid) use ($app, $user) {
            $app->contentType('application/json');
            echo($user->getAllTrips($userid));
        });


        /**
         * Randomly get users
         */
        $app->get($this->url . '/getrandom/:limit', function ($limit) use ($app, $user) {
            $app->contentType('application/json');
            echo json_encode($user->getRandomUsers($limit));
        });


        /**
         * Get user by any kind of type username,email,id
         *
         */
        $app->get($this->url . "/getby/:type/:value", function ($type, $value) use ($app, $user) {
            $app->contentType('application/json');
            switch ($type) {
                case "id":
                    echo json_encode($user->byUserID($value));
                    break;
                case "username":
                    echo json_encode($user->byUserName($value));
                    break;
                case "email":
                    echo json_encode($user->byEmail($value));
                    break;
                default:
                    break;
            }
        });


        /**
         * get user is avb on system by username
         */
        $app->get($this->url . '/validusername/:username', function ($username) use ($app, $user) {
            $app->contentType('application/json');
            /* @var $email type */
            /* @var $username type */
            $user = $user->byUserName($username);
            if ($user) {
                echo json_encode(TRUE);
            } else {
                echo json_encode(FALSE);
            }
        });

        /**
         * get user is avb on system by email
         */
        $app->get($this->url . '/validemail/:email', function ($email) use ($app, $user) {
            $app->contentType('application/json');
            /* @var $email type */
            $user = $user->byEmail($email);
            if ($user) {
                echo json_encode(TRUE);
            } else {
                echo json_encode(FALSE);
            }
        });


        /**
         * Check login details are valid or not
         *
         */
        $app->post($this->url . '/login', function () use ($app, $user) {
            //  $app->contentType('application/json');
            $userDetails = $app->request->post();
            //print_r($userDetails);
            $username = $userDetails['username'];
            $password = $userDetails['password'];


            $userDetails = $user->isValidLoginDetails($username, $password);


            $baseUrl = ($app->request->getRootUri());

            if (is_array($userDetails)) {
                $_SESSION['userid'] = $userDetails['userid'];
                $_SESSION['user'] = $userDetails;

                print_r("Login Function  2" . $_SESSION);
                $app->redirect($app->request->post('redirectPageLocation'));
            } else {
                $app->redirect($baseUrl . "/userauth?invalid=true");
            }
        });


        /**
         * Reset password for correct username
         */
        $app->post($this->url . '/resetpassword', function () use ($app, $user) {

            $userDetails = $app->request->post();

            $userDetails = $user->resetPassword($userDetails["username"]);
            print_r($userDetails);
            $emailCtrl = new EmailCtrl($app);
            $emailCtrl->sendPasswordRestMessage($userDetails);


            $app->redirect("/resetpasswordcomplete");

        });


        $app->get($this->url . '/logout', function () use ($app, $user) {

            ($_SESSION['userid'] = null);
            ($_SESSION['user'] = null);
            unset($_SESSION);
            $app->request->cookies->clear();
            session_destroy();
            $_SESSION = array();
            session_write_close();

            print_r($_SESSION);

            $baseUrl = '/';

            print_r($app->view());
            echo("<br/>");
            $app->view()->setData("user", null);
            $app->view()->setData("userid", null);
            print_r($app->view());
            //print_r($baseUrl);
            $app->redirect($baseUrl);
        });

        /**
         * Check login details are valid or not
         *
         */
        $app->post($this->url . '/register', function () use ($app, $user) {
            $app->contentType('application/json');
            $userDetails = json_decode(json_encode($app->request->post()));
            //print_r($userDetails);
            $userDetails = $user->registerNewUser($userDetails);

            $emailCtrl = new EmailCtrl($app);
            ($emailCtrl->sendConfirmAccountEmail(json_decode($userDetails)));

            //print_r($userDetails);
            $baseUrl = "/userauth";
            $app->redirect($baseUrl);
        });


        /**
         * Update profile data
         *
         */
        $app->post($this->url . '/update', function () use ($app, $user) {

            $userDetails = $app->request->post();
            //print_r($userDetails);
            //  print_r($_FILES);
            $loggedUser = null;
            $updateDone = true;

            if (array_key_exists("user", $_SESSION)) {
                $loggedUser = $_SESSION['user'];
            }


            if (is_array($loggedUser)) {


                if (array_key_exists("firstname", $userDetails)) {

                    $user->updateProfileDetails($loggedUser['username'], 'firstname', $userDetails['firstname']);

                }

                if (array_key_exists("propic", $_FILES) && !empty($_FILES['propic']) && is_array($_FILES['propic'])) {


                    // echo("upload section");


                    $tempPath = $_FILES['propic']['tmp_name'];
                    $imageName = $_FILES['propic']['name'];


                    $image = new Image();
                    $imageurl = ($image->upload($tempPath, $imageName));
                    $userid = $_SESSION['userid'];
                    $imageurl = $image->move($imageurl, "user" . DIRECTORY_SEPARATOR . $userid);


                    $user->updateProfileDetails($loggedUser['username'], 'propic', $imageurl);

                    $updateDone = true;
                }


                if (array_key_exists("password", $userDetails) && !empty($userDetails['password'])) {


                    $userArray = $user->isValidLoginDetails($loggedUser['username'], $userDetails['oldpassword']);


                    if (is_array($userArray)) {
                        $user->updateProfileDetails($loggedUser['username'], 'password', $userDetails['password']);
                    } else {
                        $app->redirect("/accountsettings?invalidpassword=true");
                    }

                }

                if ($updateDone) {
                    $_SESSION['user'] = $user->byUserID($_SESSION['userid']);
                    $app->redirect("/profile");
                }

            }


        });

    }

    public function getLoggedUserProfile()
    {
    }

}
