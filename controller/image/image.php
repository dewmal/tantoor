<?php
namespace App;
class Image {

    private $orig_dir;
    private $FILE_DIR = 'storage';

    function __construct() {
        $basedir = dirname(__FILE__);
        $path = str_replace(DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR . 'image', '', $basedir);
        $this->orig_dir = $path . DIRECTORY_SEPARATOR . $this->FILE_DIR;
        //echo $this->orig_dir;
    }

    function upload($filepath, $filename, $dir = 'tmp', $name = NULL) {
        // echo '';

        $resize = false;
        if ($dir == "tmp") {
            $resize = true;
            $dir = $dir . DIRECTORY_SEPARATOR . session_id();
        }

        $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

        if (!is_null($name)) {
            $imageName = $name . '.jpg';
        } else {
            $imageName = unique_id(20) . '.' . $ext;
        }



        $uploadPath = $this->createFolder($dir) . DIRECTORY_SEPARATOR . $imageName;
        move_uploaded_file($filepath, $uploadPath);
        if ($resize) {
            list($old_w, $old_h) = getimagesize($uploadPath);
            if ($old_h > 1024 || $old_w > 1024) {
                $this->resize($uploadPath, $uploadPath, $ext, 1024, 1024);
            } else {
                $this->resize($uploadPath, $uploadPath, $ext, $old_w, $old_h);
            }
        }

        return $this->FILE_DIR . DIRECTORY_SEPARATOR . $dir . '/' . $imageName;
    }

    function move($imagepath, $location) {
        $ext = strtolower(pathinfo($imagepath, PATHINFO_EXTENSION));
        $imageName = unique_id(20) . '.' . $ext;
        $uploadPath = $this->createFolder($location) . DIRECTORY_SEPARATOR . $imageName;
        $imagepath = str_replace($this->FILE_DIR, '', $imagepath);
        $imagepath = $this->getPath() . $imagepath;
        rename($imagepath, $uploadPath);
        return $this->FILE_DIR . DIRECTORY_SEPARATOR . $location . DIRECTORY_SEPARATOR . $imageName;
    }

    function getPath() {
        return $this->orig_dir . DIRECTORY_SEPARATOR;
    }

    /**
     * Create Dir in server storage location
     * @param type $dir
     */
    function createFolder($dir) {
        $path = $this->getPath() . $dir;
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        return $path;
    }

    /**
     * Delete Folder from dir
     * @param type $dir
     */
    function removeFolder($dir) {

        if (substr($dir, strlen($dir) - 1, 1) != '/')
            $dir .= '/';
        //echo $dir;

        if ($handle = opendir($dir)) {
            while ($obj = readdir($handle)) {
                if ($obj != '.' && $obj != '..') {
                    if (is_dir($dir . $obj)) {
                        if (!deleteDir($dir . $obj))
                            return false;
                    }
                    elseif (is_file($dir . $obj)) {
                        if (!unlink($dir . $obj))
                            return false;
                    }
                }
            }

            closedir($handle);

            if (!@rmdir($dir))
                return false;
            return true;
        }
        return false;
    }

    function removeImage($dir) {
        $path = $this->getPath();
        $dir = str_replace("storage/", "", $dir);
        //echo '<br/>'.$dir;
        $path = $path . $dir;
        //echo '<br/>'.$path;
        if (file_exists($path)) {
            if (is_dir($path)) {
                rmdir($path);
            } else {
                unlink($path);
            }
        }
    }

    function removetripImages($tripid) {
        $this->removeFolder($this->orig_dir . DIRECTORY_SEPARATOR . "trip" . DIRECTORY_SEPARATOR . $tripid);
    }

    public function resize($filename_original, $filename_resized, $extension, $new_w, $new_h) {
        // $extension = pathinfo($filename_original, PATHINFO_EXTENSION);
        // echo $extension;
        if (in_array($extension, array("jpg", "jpeg", "JPG", "JPEG"))) {
            $src_img = imagecreatefromjpeg($filename_original);
            //echo $src_img;
        }

        if (in_array($extension, array("png", "PNG")))
            $src_img = imagecreatefrompng($filename_original);

        if (!$src_img)
            return false;

        $old_w = imageSX($src_img);
        $old_h = imageSY($src_img);

        $x_ratio = $new_w / $old_w;
        $y_ratio = $new_h / $old_h;

        if (($old_w <= $new_w) && ($old_h <= $new_h)) {
            $thumb_w = $old_w;
            $thumb_h = $old_h;
        } elseif ($y_ratio <= $x_ratio) {
            $thumb_w = round($old_w * $y_ratio);
            $thumb_h = round($old_h * $y_ratio);
        } else {
            $thumb_w = round($old_w * $x_ratio);
            $thumb_h = round($old_h * $x_ratio);
        }

        $dst_img = ImageCreateTrueColor($thumb_w, $thumb_h);
        //echo $dst_img;
        imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $thumb_w, $thumb_h, $old_w, $old_h);


        if (in_array($extension, array("png", "PNG")))
            imagepng($dst_img, $filename_resized, 5);
        else
            imagejpeg($dst_img, $filename_resized, 75);


        imagedestroy($dst_img);
        imagedestroy($src_img);

        return true;
    }

}


//function unique_id($l = 8) {
//    $better_token = md5(uniqid(rand(), true));
//    $rem = strlen($better_token) - $l;
//    $unique_code = substr($better_token, 0, -$rem);
//    $uniqueid = $unique_code;
//    return $uniqueid;
//}
