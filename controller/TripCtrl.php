<?php

namespace App;

use Slim;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TripReqCtrl
{

    private $url = '/request/trip';
    private $tripCtrl;

    function __construct(Slim\Slim $app)
    {

        $tripCtrl = new TripCtrl();
        $evet_type = new EventTypeCtrl();
        $updateHistory = new UpdateHistoryCtrl();


        $app->get($this->url . "/get/:id", function ($id) use ($app, $tripCtrl) {
            $app->contentType('application/json');
            //echo $id;
            echo $tripCtrl->getEvent($id);
        });


        /**
         * Set cover photo top
         *
         */
        $app->get($this->url . "/updatehistory/:start/:limit", function ($start, $limit) use ($app, $updateHistory) {
            $app->contentType('application/json');
            $parms = $app->request->get();
            $tripid = $parms['tripid'];
            $userid = NULL;
            if ((!key_exists("userid", $parms) && !key_exists("lastupdate", $parms)) && key_exists('userid', $_SESSION)) {
                $userid = $_SESSION['userid'];
            } else if (key_exists("userid", $parms)) {
                $userid = $parms['userid'];
            }
            // echo json_encode($parms);
            echo($updateHistory->getUpdateHistory($parms['tripid'], $userid, $limit, $start));
        });


        $app->get($this->url . "/allforuser/:start/:limit", function ($start, $limit) use ($app, $tripCtrl) {
            $app->contentType('application/json');
            $parms = $app->request->get();
            //echo json_encode($parms);
            if (key_exists('userid', $_SESSION)) {
                $userid = $_SESSION['userid'];
            } else {
                $userid = NULL;
            }
            echo($tripCtrl->getByType($parms['type'], $limit, $start, $userid));
        });

        /**
         * Get all
         *
         */
        $app->get($this->url . "/all/:start/:limit", function ($start, $limit) use ($app, $tripCtrl) {
            $app->contentType('application/json');
            $parms = $app->request->get();
            //echo json_encode($parms);
            if (key_exists('userid', $_SESSION)) {
                $userid = $_SESSION['userid'];
            } else {
                $userid = NULL;
            }
            echo($tripCtrl->getByType($parms['type'], $limit, $start, $userid));
        });


        /**
         * get events by user
         *
         */
        $app->get($this->url . "/byuser/:userid/:start/:limit", function ($userid, $start, $limit) use ($app, $tripCtrl) {
            $app->contentType('application/json');
            if (array_key_exists('userid', $_SESSION) && $_SESSION['userid'] == $userid) {
                echo $tripCtrl->getByOwner($userid, array('OWNER'), $limit, $start);
            } else {
                echo $tripCtrl->getByUser($userid, array('OWNER'), $limit, $start);
            }
        });

        /**
         * get events by user with me
         *
         */
        $app->get($this->url . "/usereventwithme/:userid", function ($userid) use ($app, $tripCtrl) {
            $app->contentType('application/json');
            echo $tripCtrl->getByUserWithme($userid, $_SESSION['userid']);
        });


        /**
         * Set cover photo top
         *
         */
        $app->get($this->url . "/eventtypes", function () use ($app, $evet_type) {
            $app->contentType('application/json');
            echo $evet_type->getAll();
        });


        /**
         * Set cover photo top
         *
         */
        $app->post($this->url . "/updatecoverphototop", function () use ($app, $tripCtrl) {
            $app->contentType('application/json');
            $params = json_decode($app->request()->getBody());
            ///print_r($params);
            $tripCtrl->updateCoverPhotoTop($params->tripid, $params->coverphoto_top);
            echo json_encode($params->tripid);
        });

        /**
         * Chechk publish validation
         */
        $app->get($this->url . "/canpublish/:tripid", function ($tripid) use ($app) {
            $app->contentType('application/json');

            $locationCtrl = new LocationCtrl();
            $locationCount = $locationCtrl->getLocationCountByTrip($tripid);
            if ($locationCount != 0) {
                echo json_encode(array(
                    "valid" => TRUE,
                    "message" => "Go"
                ));
            } else {

                echo json_encode(array(
                    "valid" => FALSE,
                    "message" => "Please add alteast one location before publish"
                ));
            }
        });

        /**
         * Get Trip detials
         */
        $app->get($this->url . "/get/users/:tripid", function ($tripid) use ($app, $tripCtrl) {
            $app->contentType('application/json');
            $userid = $_SESSION['userid'];
            $tripUsers = $tripCtrl->tripUsers($tripid, $userid);
            echo($tripUsers);
        });

        /**
         * Update Trip Details
         */
        $app->put($this->url . "/update", function () use ($app, $tripCtrl) {
            $app->contentType('application/json');
            $params = $app->request()->getBody();
            $tripCtrl->updateTrip(json_decode($params));
        });


        /**
         * New Trip Details
         */
        $app->post($this->url . "/new", function () use ($app, $tripCtrl) {
            $trip = ($app->request()->post());
            if (array_key_exists("userid", $_SESSION)) {
                $tripid = $tripCtrl->newEvent($trip, $_SESSION['userid']);
                if ($tripid) {
                    $app->redirect($app->request->getRootUri()."/" . $tripid);
                }
            }
        });


        /**
         * Publish event
         */
        $app->post($this->url . "/publishevent", function () use ($app, $tripCtrl) {

            $app->contentType('application/json');
            $params = json_decode($app->request()->getBody());
            ///print_r($params);
            $tripCtrl->publishTrip($params->tripid, $_SESSION['userid'], $params->publish_type);
            echo json_encode($params->tripid);
        });


        /**
         * Un Publish event
         */
        $app->post($this->url . "/unpublish", function () use ($app, $tripCtrl) {

            $app->contentType('application/json');
            $params = json_decode($app->request()->getBody());
            ///print_r($params);
            $tripCtrl->unpublishTrip($params->tripid, $_SESSION['userid'], $params->publish_type);
            echo json_encode($params->tripid);
        });


        /**
         * Un Publish event
         */
        $app->post($this->url . "/delete/:id", function ($id) use ($app, $tripCtrl) {
            $app->contentType('application/json');
            ///print_r($params);

            $message = false;
            if (array_key_exists("userid", $_SESSION)) {
                $message = $tripCtrl->deleteEvent($id, $_SESSION['userid']);
                if ($message) {
                    $image = new Image();
                    $image->removetripImages($id);
                }
                echo($message);
            }

            if ($message) {
                echo("Valid access");
            } else {
                echo("Invalid access");
            }


        });

    }

}
