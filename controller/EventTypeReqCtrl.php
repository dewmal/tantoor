<?php
namespace App;

use Slim;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class EventTypeReqCtrl {

    private $url = '/request/eventtype';
    private $eventTypeCtrl;

    function __construct(Slim\Slim $app) {


        $eventTypeCtrl = new EventTypeCtrl();

        $app->get($this->url."/get/:id",function($id) use($app,$eventTypeCtrl){
            $app->contentType('application/json');
            echo json_encode($eventTypeCtrl->get($id));
        });


    }

}
