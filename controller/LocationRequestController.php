<?php

namespace App;

use Slim;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LocationHandler {

    function __construct(\Slim\Slim $app) {

        $url = '/location';



        $app->put($url . '/save', function () use ($app) {
            $app->contentType('application/json');
            $params=null;
            $locationCtrl = new LocationCtrl();
            //echo json_encode($locationCtrl->saveLocation($parms));


        });

        $app->get($url . '/getcountbytrip/:tripid', function($tripid) {
            header('Content-Type: application/json');
            $locationCtrl = new LocationCtrl();
            echo json_encode($locationCtrl->getLocationCountByTrip($tripid));
        });



        /**
         * Delete location
         * 
         * true success
         * 
         * false invalid access
         * 
         */
        $app->post($url . '/deletelocation/', function() use($app) {
            $app->contentType('application/json');
            $locationCtrl = new LocationCtrl();
            $locationid = $app->request()->post('locationid');
            if (key_exists("userid", $_SESSION)) {
                $userid = $_SESSION['userid'];
                echo json_encode($locationCtrl->deleteLocation($locationid, $userid));
            } else {
                echo 'invalid access';
            }
        });
    }

}
