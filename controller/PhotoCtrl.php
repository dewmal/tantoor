<?php
namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PhotoReqCtrl
{

    private $url = "/request/photo";

    function __construct(\Slim\Slim $app)
    {
        // header('Content-Type: application/json');
        $photoCtrl = new PhotCtrl();

        $app->get($this->url . '/getcountbytrip/:tripid', function ($tripid) use ($app) {
            $app->contentType('application/json');
            $photoCtrl = new PhotCtrl();
            echo json_encode($photoCtrl->getPhotoCountByTripId($tripid));
        });


        $app->get($this->url . '/getphotosbyevent/:tripid/:start/:limit', function ($tripid, $start, $limit) use ($app) {
            $app->contentType('application/json');
            $photoCtrl = new PhotCtrl();
            echo json_encode($photoCtrl->getPhotoByEvent($tripid, $start, $limit, false));
            //echo json_encode($photoCtrl->getPhotoCountByTripId($tripid));
        });


        /**
         * Generate Code for forum
         */
        $app->get($this->url . '/getphotosbyeventforumcodegen/:tripid', function ($tripid) use ($app) {
            $photoCtrl = new PhotCtrl();
            $tripCtrl = new TripCtrl();
            $trip = json_decode($tripCtrl->getEvent($tripid));
            $title = $trip->name;
            $content = $trip->description;
            $code = "[color=red][b][size=+4]" . $title . "[/size][/b][/color]
            [indent][quote][size=+2]" . $content . "[/size][/quote][/indent]";

            foreach ($photoCtrl->getPhotoByEventAll($tripid) as  $value) {
                $code .= "[left][size=+1]".$value['title']."".$value['description']."[/size][/left][url=http://tantoor.com/" . $tripid . "]
                [img]http://tantoor.com/image/timthumb.php?src=" . $value['url'] . "&w=800&zc=1&q=90 [/img] [/url]";
            }
            echo($code);
        });


        $app->post($this->url . '/update', function () use ($app,$photoCtrl) {
            if ($app->request()->isPost()) {
                $photo =$app->request->getBody();
                #print_r(json_decode($photo));
                $photoCtrl->updatePhoto(json_decode($photo));
            }
        });


        /**
         * Upload cover photo and set it to event
         *
         */
        $app->post($this->url . '/coverphotoupload', function () use ($app, $photoCtrl) {
                //
                $app->contentType('application/json');


                if ($app->request()->isPost()) {


                    // print_r($app->request->post());
                    $tempPath = $_FILES['file']['tmp_name'];
                    $imageName = $_FILES['file']['name'];
                    $imageType = $_FILES['file']['type'];


                    $image = new Image();
                    $imageurl = ($image->upload($tempPath, $imageName));
                    $tripid = $app->request->post('tripid');
                    $userid = $_SESSION['userid'];
                    $imageurl = $image->move($imageurl, "trip" . DIRECTORY_SEPARATOR . $tripid);

                    $photoJson = json_encode($photoCtrl->savePhoto($imageurl, $tripid, $userid));
                    $coverPhoto = json_decode($photoJson);
                    //print_r($coverPhoto);

                    $evtCtrl = new TripCtrl();
                    $evtCtrl->updateCoverPhotoSrc(
                        $coverPhoto->url,
                        $coverPhoto->trip);

                    echo json_encode($coverPhoto);
                }


            }

        );

        /**
         * Upload Photos to server
         *
         */
        $app->post($this->url . '/upload', function () use ($app,$photoCtrl) {
                //
                $app->contentType('application/json');

// print_r($app->request->post());
                $tempPath = $_FILES['file']['tmp_name'];
                $imageName = $_FILES['file']['name'];
                $imageType = $_FILES['file']['type'];


                $image = new Image();
                $imageurl = ($image->upload($tempPath, $imageName));
                $tripid = $app->request->post('tripid');
                $userid = $_SESSION['userid'];
                $imageurl = $image->move($imageurl, "trip" . DIRECTORY_SEPARATOR . $tripid);

                $photoJson = json_encode($photoCtrl->savePhoto($imageurl, $tripid, $userid));
                echo($photoJson);
            }

        );

    }


}

function UploadPhoto(\Slim\Slim $app, PhotCtrl $photoCtrl)
{


}