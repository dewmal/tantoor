<?php

namespace App;

use Slim;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CommentReqCtrl {

    private $url = "/comment";

    function __construct(\Slim\Slim $app) {


        $commentCtrl = new CommentCtrl();

        $app->get($this->url . '/get/:type', function($type) use ($app, $commentCtrl) {
            $app->contentType('application/json');
            $id = $app->request()->get("id");
            switch ($type) {
                case "event":
                    echo $commentCtrl->getEventComments($id);

                    break;

                default:
                    break;
            }
        });

        $app->post($this->url . '/save/:type', function($type) use ($app, $commentCtrl) {
            $app->contentType('application/json');
            $params = json_decode($app->request->getBody());
            $id = $params->id;
            $comment = $params->comment;
            $userid = $_SESSION['userid'];
            switch ($type) {
                case "event":
                    echo $commentCtrl->saveComment($userid, $id, $comment);

                    break;

                default:
                    break;
            }
        });
    }

}
