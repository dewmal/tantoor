<?php

namespace App;

use Slim;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LikeReqCtrl {

    private $url = "/like";

    function __construct(\Slim\Slim $app) {


        $likeCtrl = new LikeCtrl();

        /**
         * Get user 
         */
        $app->get($this->url . '/userlikes/:id', function($id) use($app, $likeCtrl) {
            $app->contentType('application/json');
            $app->getLog()->emergency($id);
            echo ($likeCtrl->getUserLikesFor($id));
        });



        /**
         * GEt likes count for trip
         */
        $app->get($this->url . '/likecount/:id', function($id) use($app) {
            $app->contentType('application/json');
            $likeCtrl = new LikeCtrl();
            echo json_encode($likeCtrl->getLikeCount($id));
        });

        /**
         * GEt likes for trip
         */
        $app->get($this->url . '/fortrip/:id', function($id) use($app, $likeCtrl) {
            $app->contentType('application/json');
            echo ($likeCtrl->getLikesFormTrip($id));
        });


        /**
         * GEt likes for trip
         */
        $app->get($this->url . '/userlikefortrip/:tripid', function($tripid) use($app, $likeCtrl) {
            $app->contentType('application/json');
            $params = $app->request->get();
            $userid = null;
            if (key_exists('userid', $_SESSION)) {
                if (key_exists('userid', $params)) {
                    $userid = $params['userid'];
                } else {
                    $userid = $_SESSION['userid'];
                }
                echo json_encode($likeCtrl->getUserLikeOrNot($tripid, $userid));
            } else {
                echo json_encode(FALSE);
            }
        });


        $app->post($this->url . '/like', function() use($app, $likeCtrl) {
            $app->contentType('application/json');
            $params = json_decode($app->request->getBody());

            $userid = null;
            if (property_exists($params, 'userid')) {
                $userid = $params->userid;
            } else {
                $userid = $_SESSION['userid'];
            }

            echo json_encode($likeCtrl->save($params->tripid, $userid));
        });

        $app->post($this->url . '/dislike/', function() use($app, $likeCtrl) {
            $app->contentType('application/json');
            $params = json_decode($app->request->getBody());

            $userid = null;
            if (property_exists($params, 'userid')) {
                $userid = $params->userid;
            } else {
                $userid = $_SESSION['userid'];
            }

            echo json_encode($likeCtrl->delete($params->tripid, $userid));
        });
    }

}
