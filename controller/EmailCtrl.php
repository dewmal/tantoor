<?php
/**
 * Created by PhpStorm.
 * User: dewmal
 * Date: 3/21/14
 * Time: 4:20 PM
 */

namespace App;


class EmailCtrl
{

    private $sendgrid;
    private $email;

    function __construct(\Slim\Slim $app)
    {
        $options = array("turn_off_ssl_verification" => true);
        $this->sendgrid = new \SendGrid('dewmal', 'dewmal91', $options);
        $this->email = new \SendGrid\Email();
    }


    public function sendConfirmAccountEmail($userDetails)
    {


        try {
            $username = $userDetails->username;
            $confirmationid = $userDetails->confirm;
            $url = "http://tantoor.com/thanksconfrimmail?confirm=". $confirmationid ."&username=" . $username;
            //$url = "http:///confimation?code=" + $confirmationid + "&user=" + $username;
            $msg = "Dear $username<br><br><br>"
                . "Thank you for registering at the tantoor. Before we can activate your account one
                last step must be<br> taken to complete your registration.<br><br>"
                . "Please note - you must complete this last step to become a registered member.
                You will only need to visit this url once to activate your account.<br><br>"
                . "To complete your registration, please visit this url:<br>"
                . "<a href='$url'>Activate profile</a><br><br>"
                ."If you have any problem with this url please use this"
                ." <a href='http://tantoor.com/confimation'>Confirm email by manual</a><br><br> "
                . "Please be sure not to add extra spaces. You will need to type in your username
                and activation number on the page that appears when you visit the url.<br><br>"
                . "Your username is: $username<br>Your Activation ID is: $confirmationid<br><br>"

                . "All the best<br>"
                . "tantoor.com";


            $this->email->
                addTo($userDetails->email)->
                setFrom('noreply@tantoor.com')->
                setSubject('Action Required to Activate Membership for tantoor.com')->
                setHtml($msg);

            print_r($this->email);

            $response = ($this->sendgrid->send($this->email));
            print_r($response);
        } catch (Exception $e) {
            print_r($e);
        }
    }


    /**
     *
     * Send password reset message to user
     *
     * @param $userDetails
     */
    public function sendPasswordRestMessage($userDetails)
    {

        try {
            $username = $userDetails['username'];
            $password = $userDetails['password'];
            $msg = "Hi " . $username . "<br><br><br>"
                . "Your password reset new password is : " . $password
                . "<p></p>Thank you <p></p>tantoor.com";


            $this->email->
                addTo($userDetails['email'])->
                setFrom('noreply@tantoor.com')->
                setSubject('Your new password')->
                setHtml($msg);


            $response = ($this->sendgrid->send($this->email));
            print_r($response);
        } catch (Exception $e) {
            print_r($e);
        }
    }
}