<?php

namespace App;

use Slim;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class UserHasEventReqCtrl {

    private $url = "/request/userhasevent";

    function __construct(Slim\Slim $app) {

        $userhaseventCtrl = new UserEventsEntityCtrl();


        $app->post($this->url . "/save", function() use($app, $userhaseventCtrl) {
            $app->contentType('application/json');
            $eventhasuser = json_decode($app->request->getBody());
            $userhaseventCtrl->save($eventhasuser->tripid, $eventhasuser->userid, $eventhasuser->position);
            echo json_encode('SUCCESS');
        });

        $app->put($this->url . "/update", function() use($app, $userhaseventCtrl) {
            $app->contentType('application/json');
            $eventhasuser = json_decode($app->request->getBody());
            $userhaseventCtrl->update($eventhasuser->tripid, $eventhasuser->userid, $eventhasuser->position);
            echo json_encode('SUCCESS');
        });

        $app->post($this->url . "/delete", function() use($app, $userhaseventCtrl) {
            $app->contentType('application/json');
            $eventhasuser = json_decode($app->request->getBody());
            $delete = $userhaseventCtrl->delete($eventhasuser->tripid, $eventhasuser->userid);
            echo json_encode('SUCCESS');
        });


        $app->get($this->url . "/all/:tripid", function($tripid) use($app, $userhaseventCtrl) {
            $app->contentType('application/json');
            echo $userhaseventCtrl->getAllUsersByTrip($tripid);
        });
    }

}
