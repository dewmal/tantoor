<?php

namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FbCtrl {

    private $url = "/fb";

    function __construct(\Slim\Slim $app) {

        $fbCtrl = new FBActvityCtrl($app);


        $app->get($this->url . '/post/photo/:photoid', function($photoid) use ($app, $fbCtrl) {
            $app->contentType('application/json');
            $photoCtrl = new PhotCtrl();
            $photo = $photoCtrl->getById($photoid);
            $fbCtrl->postPhoto($photo);
        });


        $app->get($this->url . '/post/status/', function($message) use ($app, $fbCtrl) {
            $app->contentType('application/json');
            $fbCtrl->postMessage($message, "TEst");
        });


        $app->get($this->url . '/login', function () use ($app, $fbCtrl) {

            //  echo("done");
            try {

                if ($fbCtrl->loginToFb()) {


                    $user = $fbCtrl->getUserData();


                    $userReqCtrl = new UserCtrl();

                    $userDetails = $userReqCtrl->byEmail($user['email']);

                    if (!is_array($userDetails)) {

                        $userDetails = new \stdClass();
                        $userDetails->email = $user['email'];
                        $userDetails->gender = $user['gender'];
                        $userDetails->firstname = $user['first_name'];
                        $userDetails->password = unique_id(6);


                        $userReqCtrl->registerNewUser($userDetails);
                        $userReqCtrl->updateProfileDetails($userDetails->email, 'fbid', $user['id']);


                    }

                    //  print_r($user);
                    //  print_r($userDetails);

                    $fbCtrl->postMessage("Hey this is awesome for share events with your, check this now
                     #tantoor #event
                     #fun #friends #party #trip
                     #hike #birthday #batch
                      #‎voyage‬ #travel
                      #gettogether #conference ", "http://tantoor.com");
                    //
                    try {


                        $picture = $user['picture'];
                        $pictureDate = $picture['data'];

                        echo "test";
                        $userReqCtrl->updateProfileDetails($user['email'], 'propic', $pictureDate['url']);
                        echo "test";
                        $userDetails = $userReqCtrl->byEmail($user['email']);
                        echo "test";
                        $_SESSION['userid'] = $userDetails['userid'];
                        $_SESSION['user'] = $userDetails;

                        echo "test";
                        $app->redirect("http://tantoor.com/profile");
                    } catch (\Exception $e) {
                        print_r($e);
                    }


                }

            } catch (\Exception $e) {
                printf($e);
            }
        });


        $app->get($this->url . '/post/share/', function () use ($app, $fbCtrl) {
            $messageDetails = $app->request->get();
            $url = $app->request->getUrl() . "/" . $messageDetails['eventid'];
            if ($fbCtrl->loginToFb()) {
                print_r($messageDetails);
                $fbCtrl->postMessage($messageDetails['message'], $url);
            }
            $app->redirect($url);
        });


        $app->get($this->url . '/friends', function () use ($app, $fbCtrl) {

            $name = $app->request->get("data");

            $app->contentType('application/json');
            $data = $fbCtrl->findUsers();
            $data = $data['data'];
            $dataFilter = array();

            foreach ($data as $friend) {
                $name_fb = $friend['name'];

                if (strpos(strtolower($name_fb), strtolower($name)) !== false) {
                    array_push($dataFilter, $friend);
                }
            }

            echo json_encode($dataFilter);
        });

    }

}
/**
 * 
 * Controller Calss for Facebook functions
 * 
 */
class FBActvityCtrl {

    private $app_id = "622320784484154";
    private $app_secret = "1f92baf2828296785133a7df0959bda9";
    private $post_login_url = "YOUR_POST_LOGIN_URL";
    private $facebook;
    private $app;


    function __construct(\Slim\Slim $app)
    {

        $this->app = $app;

        $config = array(
            'appId' => $this->app_id,
            'secret' => $this->app_secret,
            'fileUpload' => true,
            'allowSignedRequest' => false // optional but should be set to false for non-canvas apps
        );
        $this->facebook = new \Facebook($config);
    }

    public function loginToFb()
    {
        $params = array(
            'scope' => 'read_stream, friends_likes, email,email,publish_stream,publish_actions'
        );
        //

        $user_id = $this->facebook->getUser();
        //echo($user_id);
        if (!$user_id) {
            echo($user_id);
            $login_url = $this->facebook->getLoginUrl($params);
            $this->app->redirect($login_url);
        } else {
            //echo($user_id);
            return TRUE;
        }
    }

    /**
     * Post photo on facebook
     * @param type $param
     */
    public function postMessage($message, $link, $picture="http://tantoor.com/static/img/loginbg2.jpg")
    {

        if ($this->loginToFb()) { {
                try {
                    $ret_obj = $this->facebook->api('/me/feed', 'POST', array(
                        'message' => $message,
                        "link" => $link,
                        "picture" => $picture
                    ));
                } catch (FacebookApiException $e) {
                    $this->loginToFb();
                }
            }
        }
    }


    /**
     * Find user details
     *
     * @return mixed
     */
    public function getUserData()
    {
        $user = $this->facebook->api("/me?fields=first_name,last_name,gender,email,picture.type(large).height(500)", "GET");
        // print_r($user);
        return $user;
    }


    /**
     * Post photo on facebook
     * @param type $param
     */
    public function postPhoto(array $photo) {

        if ($this->loginToFb()) { {
                try {
                    $ret_obj = $this->facebook->api('/me/photos', 'POST', array(
                        'source' => '@../' . $photo['url'],
                        'message' => $photo['description'],
                        'name' => $photo['title'],
                            )
                    );
                } catch (FacebookApiException $e) {
                    $this->loginToFb();
                }
        }
        }


    }


    /**
     * Post photo on facebook
     * @param type $param
     */
    public function findUsers()
    {

        if ($this->loginToFb()) {
            {

                try {
                    $friends = $this->facebook->api("/me/friends?fields=name,gender,email,picture.type(small)", "GET");
                    return $friends;
                } catch (FacebookApiException $e) {
                    print_r($e);
                    $this->loginToFb();
                }
            }
        }
    }

}
